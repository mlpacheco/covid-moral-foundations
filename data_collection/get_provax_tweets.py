from tqdm import tqdm
import json
import csv
import os

def main():
    # First extract positive hashtags
    pro_hashtags = set()
    anti_hashtags = set()
    directory = "/scratch3/data/CovidTweets/hashtags/aggregated_hashtags_annotated.csv"
    with open(directory) as fp:
        reader = csv.reader(fp)
        for i, row in enumerate(reader):
            if i == 0:
                continue
            [hashtags, _, pro, con] = row
            if pro == "TRUE":
                pro_hashtags.add(hashtags)
            if con == "TRUE":
                anti_hashtags.add(hashtags)

    # Then try to find tweets that map to these
    print(pro_hashtags)
    #print(anti_hashtags)

    tweet_ids = set(); all_tweets = {'tweets': [], 'hashtags': pro_hashtags}
    directory = "/scratch3/data/CovidTweets/2021_vaccine_tweets"
    for filename in os.listdir(directory):
        filepath = os.path.join(directory, filename)
        with open(filepath) as fp:
            tweets = json.load(fp)
            pbar = tqdm(desc=filename, total=len(tweets['tweets']))
            for tw in tweets['tweets']:
                #print(tw['entities'].keys())
                #print(tw['entities']['mentions'])
                if 'entities' in tw and 'hashtags' in tw['entities']:
                    for hashtag in tw['entities']['hashtags']:
                        if hashtag['tag'] in pro_hashtags:
                            #print(hashtag['tag'])
                            #print(tw['text'])
                            tweet_ids.add(tw['id'])
                pbar.update(1)
            pbar.close()
            #print(len(tweet_ids))
    print(len(tweet_ids))

if __name__ == "__main__":
    main()
