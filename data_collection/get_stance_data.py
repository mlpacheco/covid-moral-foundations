import json
import argparse
import datetime
from twarc.client2 import Twarc2
from twarc.expansions import ensure_flattened
from tqdm import tqdm
import os
import urllib


def main(args, username_list, stance):
    # Your bearer token here
    config = json.load(open(args.config))
    t = Twarc2(consumer_key=config['consumer_key'], consumer_secret=config['consumer_secret'])
    #if os.path.isfile("/scratch3/data/CovidTweets/{}_vaccine_tweets_by_username.json".format(stance)):
    #    output = json.load(open("/scratch3/data/CovidTweets/{}_vaccine_tweets_by_username.json".format(stance), encoding='utf-8'))
    #else:
    output = {}

    for username in username_list:

        # initial query, we might want to expand later
        query = 'from:{} ((covid vaccine) OR vaccine) lang:en -is:retweet'.format(username)
        # print(query)

        if username not in output:
            output[username] = {'tweets': [], 'stance': stance}

        # print("Loaded initial file with {0} users".format(len(output)))
        try:
            search_results = t.search_all(query=query, max_results=100)

            n_tweets = 0
            pbar = tqdm(total=100000, desc='downloading tweets for {}'.format(username))
            for page in search_results:
                for tweet in ensure_flattened(page):
                    # print(tweet)
                    output[username]['tweets'].append(tweet)
                    n_tweets += 1
                    pbar.update(1)
                # Up to 100,000 per username (I'm sure it will be way less)
                if n_tweets >= 100000:
                    break
            pbar.close()

            with open('/scratch3/data/CovidTweets/vaccine_stance_tweets/{}_vaccine_tweets_by_username.json'.format(stance), "w", encoding='utf-8') as fp:
                json.dump(output, fp)
            print("Wrote {0} tweets username {1}".format(n_tweets, username))
        except urllib.error.HTTPError:
            print(f"Failed to open: {username}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', required=True, type=str)
    args = parser.parse_args()

    # TO-DO: load the list of usernames
    for stance in ['pro', 'anti']:
        f = open("data_collection/{}_vaccine.txt".format(stance), "r")
        username = []
        for file in f:
            currUsername = file[file.rfind('/') + 1: len(file)]
            currUsername = currUsername.replace('\n', '')
            username.append(currUsername)

        # print(f.read())
        main(args, username, stance)
