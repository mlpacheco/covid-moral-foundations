import json
import argparse
from transformers import AutoTokenizer
from tqdm import tqdm
import os
import wikipedia
from collections import Counter
import networkx as nx
from sentence_transformers import SentenceTransformer
from scipy.spatial import distance
import numpy as np

def entity_group_lexform(entity2lexform, entity2tweet, dataset, covid_data):
    all_entities = []; lexform2ids ={}; tweet_groups = {}
    pbar = tqdm(total=len(entity2lexform), desc='grouping entities based on lexform')
    for ent in entity2lexform:
        lexform = entity2lexform[ent].lower()
        all_entities.append(lexform)
        if lexform not in lexform2ids:
            lexform2ids[lexform] = []
        lexform2ids[lexform].append(ent)
        pbar.update(1)
    pbar.close()
    for group, lexform in enumerate(Counter(all_entities)):
        for idx in lexform2ids[lexform]:
            dataset['entity_group_lexform'].add((str(idx), str(group)))
            if entity2tweet[idx] not in tweet_groups:
                tweet_groups[entity2tweet[idx]] = set()
            tweet_groups[entity2tweet[idx]].add(str(group))

    find_connected_components(tweet_groups, dataset, covid_data, 'in_instance_lexform')

def entity_group_wikipedia(entity2lexform, entity2tweet, dataset, covid_data):
    all_entities = []; wikipedia2ids = {}; tweet_groups = {}
    pbar = tqdm(total=len(entity2lexform), desc='grouping entities based on wikipedia')
    for ent in entity2lexform:
        lexform = entity2lexform[ent]
        ret = wikipedia.search(lexform)
        if len(ret) > 0:
            ret = ret[0]
        else:
            ret = lexform
        if ret not in wikipedia2ids:
            wikipedia2ids[ret] = []
        wikipedia2ids[ret].append(ent)
        all_entities.append(ret)
        pbar.update(1)
    pbar.close()
    for group, lexform in enumerate(Counter(all_entities)):
        for idx in wikipedia2ids[lexform]:
            dataset['entity_group_wikipedia'].add((str(idx), str(group)))
            if entity2tweet[idx] not in tweet_groups:
                tweet_groups[entity2tweet[idx]] = set()
            tweet_groups[entity2tweet[idx]].add(str(group))
    find_connected_components(tweet_groups, dataset, covid_data, 'in_instance_wikipedia')

def find_connected_components(tweet_groups, dataset, covid_data, filename):
    g = nx.Graph()
    for tw in tweet_groups:
        g.add_node(tw)
        for tw_2 in tweet_groups:
            if tw != tw_2 and len(tweet_groups[tw] & tweet_groups[tw_2]) > 0:
                if not g.has_node(tw_2):
                    g.add_node(tw_2)
                g.add_edge(tw, tw_2)
    seen_tweets = set()
    n_component = 0
    for i, connected_comp in enumerate(nx.connected_components(g)):
        for tw in connected_comp:
            dataset[filename].add((str(tw), str(n_component)))
            seen_tweets.add(tw)
        n_component += 1
    for tw in covid_data:
        if tw not in seen_tweets:
            dataset[filename].add((str(tw), str(n_component)))
            n_component += 1

def compute_theme_embeddings(model, themes):
    theme_embeddings = {}
    for theme in themes:
        embeddings = []
        for phrase in themes[theme]:
            embeddings.append(model.encode([phrase.lower()])[0])
        theme_embeddings[theme] = embeddings
    return theme_embeddings

def get_tweet_theme(model, tweet, themes, theme_embeddings):
    tweet_embed = model.encode([tweet])[0]
    min_dis = []; curr_themes = []
    for theme, val in themes.items():
        dis = []
        for i in range (0, len(val)): #phrases in each theme
            dis.append(distance.cdist(tweet_embed.reshape(1 , 768), theme_embeddings[theme][i].reshape(1 , 768), 'cosine'))
        min_dis.append(np.array(dis).min(axis=0).item())
        curr_themes.append(theme)
    min_index = np.array(min_dis).argmin(axis=0).item()
    min_dist = min_dis[min_index]
    theme = curr_themes[min_index]
    return theme, min_dist

def main(args):
    dataset = {
        'is_tweet': set(),
        'has_entity': set(),
        'has_mf': set(),
        'has_morality': set(),
        'has_role': set(),
        'has_sentiment': set(),
        'has_stance': set(),
        'has_theme_0.3': set(),
        'has_theme_0.5': set(),
        'has_theme': set(),
        'role_label': set(),
        'sentiment_label': set(),
        'morality_label': set(),
        'mf_label': set(),
        'stance_label': set(),
        'entity_group_lexform': set(),
        'entity_group_wikipedia': set(),
        'in_instance_lexform': set(),
        'in_instance_wikipedia': set(),
        'has_stance_implicit': set(),
        'has_stance_explicit': set()
    }

    tokenizer = AutoTokenizer.from_pretrained('bert-base-uncased')
    sbert_model = SentenceTransformer('all-mpnet-base-v2')

    themes = json.load(open(args.theme_path))
    theme_embeddings = compute_theme_embeddings(sbert_model, themes)

    entity_idx = 0
    tweet2bert = {}; entity2bert = {}; entity2lexform = {}; entity2tweet = {}
    with open(args.covid_path) as fp:
        covid_data = json.load(fp)
        pbar = tqdm(total=len(covid_data))

        dataset['role_label'].add(('actor',))
        dataset['role_label'].add(('target',))
        dataset['sentiment_label'].add(('sentiment-pos',))
        dataset['sentiment_label'].add(('sentiment-neg',))
        dataset['morality_label'].add(('moral',))
        dataset['morality_label'].add(('non-moral',))

        for tw_id in covid_data:
            dataset['is_tweet'].add((tw_id,))
            tweet_bert = tokenizer(covid_data[tw_id]['text'], padding='max_length', truncation=True, max_length=100)
            tweet2bert[tw_id] = dict(tweet_bert)

            theme, dist = get_tweet_theme(sbert_model, covid_data[tw_id]['text'].lower(),
                                           themes, theme_embeddings)
            if dist < 0.3:
                dataset['has_theme_0.3'].add((str(tw_id), theme))
            elif dist < 0.5:
                dataset['has_theme_0.5'].add((str(tw_id), theme))
            dataset['has_theme'].add((str(tw_id), theme))

            for actor in covid_data[tw_id]['pos_actors']:
                dataset['has_role'].add((tw_id, str(entity_idx), 'actor'))
                dataset['has_sentiment'].add((tw_id, str(entity_idx), 'sentiment-pos'))
                dataset['has_entity'].add((tw_id, str(entity_idx)))

                tweet_entity_bert = tokenizer(covid_data[tw_id]['text'], actor, padding='max_length', truncation=True, max_length=100)
                entity2bert[entity_idx] = dict(tweet_entity_bert)

                entity2lexform[entity_idx] = actor
                entity2tweet[entity_idx] = tw_id

                entity_idx += 1

            for actor in covid_data[tw_id]['neg_actors']:
                dataset['has_role'].add((tw_id, str(entity_idx), 'actor'))
                dataset['has_sentiment'].add((tw_id, str(entity_idx), 'sentiment-neg'))
                dataset['has_entity'].add((tw_id, str(entity_idx)))

                tweet_entity_bert = tokenizer(covid_data[tw_id]['text'], actor, padding='max_length', truncation=True, max_length=100)
                entity2bert[entity_idx] = dict(tweet_entity_bert)

                entity2lexform[entity_idx] = actor
                entity2tweet[entity_idx] = tw_id

                entity_idx += 1

            for target in covid_data[tw_id]['pos_targets']:
                dataset['has_role'].add((tw_id, str(entity_idx), 'target'))
                dataset['has_sentiment'].add((tw_id, str(entity_idx), 'sentiment-pos'))
                dataset['has_entity'].add((tw_id, str(entity_idx)))

                tweet_entity_bert = tokenizer(covid_data[tw_id]['text'], target, padding='max_length', truncation=True, max_length=100)
                entity2bert[entity_idx] = dict(tweet_entity_bert)

                entity2lexform[entity_idx] = target
                entity2tweet[entity_idx] = tw_id

                entity_idx += 1

            for target in covid_data[tw_id]['neg_targets']:
                dataset['has_role'].add((tw_id, str(entity_idx), 'target'))
                dataset['has_sentiment'].add((tw_id, str(entity_idx), 'sentiment-neg'))
                dataset['has_entity'].add((tw_id, str(entity_idx)))

                tweet_entity_bert = tokenizer(covid_data[tw_id]['text'], target, padding='max_length', truncation=True, max_length=100)
                entity2bert[entity_idx] = dict(tweet_entity_bert)

                entity2lexform[entity_idx] = target
                entity2tweet[entity_idx] = tw_id

                entity_idx += 1


            if covid_data[tw_id]['mf'] is not None:
                mf = covid_data[tw_id]['mf'].strip().replace("sanctity", "purity")
                dataset['has_mf'].add((tw_id, mf))
                dataset['mf_label'].add((mf,))
                if mf == "none":
                    dataset['has_morality'].add((tw_id, 'non-moral'))
                else:
                    dataset['has_morality'].add((tw_id, 'moral'))

            if covid_data[tw_id]['stance'] is not None and covid_data[tw_id]['stance'] not in [2, "2"]:
                dataset['has_stance'].add((tw_id, str(covid_data[tw_id]['stance'])))
                dataset['stance_label'].add((str(covid_data[tw_id]['stance']),))

                if covid_data[tw_id]['stance_comments'] == "":
                    dataset['has_stance_explicit'].add((tw_id, str(covid_data[tw_id]['stance'])))
                else:
                    dataset['has_stance_implicit'].add((tw_id, str(covid_data[tw_id]['stance'])))


            pbar.update(1)
        pbar.close()

    with open(os.path.join(args.out_dir_bert, 'tweet2bert.json'), 'w') as fp:
        json.dump(tweet2bert, fp)
    with open(os.path.join(args.out_dir_bert, 'entity2bert.json'), 'w') as fp:
        json.dump(entity2bert, fp)
    with open(os.path.join(args.out_dir_bert, 'entity2lexform.json'), 'w') as fp:
        json.dump(entity2lexform, fp)

    entity_group_lexform(entity2lexform, entity2tweet, dataset, covid_data)
    entity_group_wikipedia(entity2lexform, entity2tweet, dataset, covid_data)

    for predicate in dataset:
        with open(os.path.join(args.out_dir_drail, predicate + '.txt'), 'w') as fp:
            for tup in dataset[predicate]:
                fp.write('\t'.join(tup))
                fp.write('\n')

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--covid_path', type=str, required=True)
    parser.add_argument('--theme_path', type=str, required=True)
    parser.add_argument('--out_dir_bert', type=str, required=True)
    parser.add_argument('--out_dir_drail', type=str, required=True)
    args = parser.parse_args()
    main(args)
