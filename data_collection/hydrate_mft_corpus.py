import json
import argparse
import datetime
from twarc.client2 import Twarc2
from twarc.expansions import ensure_flattened
from tqdm import tqdm
import os

def main(args):
    # Your bearer token here
    config = json.load(open(args.config))
    t = Twarc2(consumer_key=config['consumer_key'], consumer_secret=config['consumer_secret'])

    annotations = json.load(open('/scratch1/pachecog/covid/MFTC_V4.json'))
    '''
    results = {}
    for ann in annotations:
        print(ann['Corpus'])
        tweet_ids = []
        pbar = tqdm(total=len(ann['Tweets']))
        for tw in ann['Tweets']:
            tweet_id = tw['tweet_id']
            tweet_ids.append(tweet_id)
        res = t.tweet_lookup(tweet_ids)
        for elem in res:
            if 'data' in elem:
                for hyd in elem['data']:
                    results[hyd['conversation_id']] = hyd
                    pbar.update(1)
            else:
                print(elem.keys())
        pbar.close()

    with open("/scratch1/pachecog/covid/MFTC_V4_hydrated.json", "w") as fp:
        json.dump(results, fp)
    '''

    hydrated = json.load(open("/scratch1/pachecog/covid/MFTC_V4_hydrated.json"))

    for ann in annotations:
        for tw in ann['Tweets']:
            tweet_id = tw['tweet_id']
            if tweet_id in hydrated:
                tw['text'] = hydrated[tweet_id]['text']

    with open('/scratch1/pachecog/covid/MFTC_V4.json', "w") as fp:
        json.dump(annotations, fp)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', required=True, type=str)
    parser.add_argument('--start_month', type=int, default=1)
    parser.add_argument('--period_day', type=int, default=10)
    args = parser.parse_args()
    main(args)
