import os
import json
from tqdm import tqdm

def main():
    all_tweets = {'streaming': []}
    tweet_ids = set()
    directory = "/homes/pachecog/avax-tweets-dataset/streaming-tweetids/"
    for subdir_name in os.listdir(directory):
        subdir = os.path.join(directory, subdir_name)
        if not os.path.isdir(subdir):
            continue
        files = os.listdir(subdir)
        files = [f for f in files if f.endswith('jsonl.gz') or f.endswith('jsonl')]
        pbar = tqdm(total=len(files), desc=subdir_name)
        for filename in files:
            filepath = os.path.join(subdir, filename)
            if filepath.endswith('jsonl.gz'):
                # uncompress file if needed
                uncompressed = filepath.replace('jsonl.gz', 'jsonl')
                if not os.path.exists(uncompressed):
                    os.system("gunzip {}".format(filepath))
            else:
                uncompressed = filepath

            # readfile
            with open(uncompressed) as fp:
                for line in fp:
                    tweets = json.loads(line)
                    if 'includes' in tweets and 'tweets' in tweets['includes']:
                        for i in range(0, len(tweets['includes']['tweets'])):
                            if tweets['includes']['tweets'][i]['id'] not in tweet_ids:
                                all_tweets['streaming'].append(tweets['includes']['tweets'][i])
                                tweet_ids.add(tweets['includes']['tweets'][i]['id'])
                    #all_tweets['streaming'].append(tweet)
                    #print(tweet.keys())
                    #exit()
            pbar.update(1)
        pbar.close()
    with open("/scratch3/data/CovidTweets/avax_tweets.json", "w") as fp:
        json.dump(all_tweets, fp)

    print("Wrote {1} tweets, out of which {0} are unique".format(len(tweet_ids), len(all_tweets['streaming'])))

if __name__ == "__main__":
    main()
