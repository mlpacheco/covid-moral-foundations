import json
from collections import Counter
import os

sentiment_words_all = {'positive': [], 'negative': []}
moral_words_all = []

PATH = '/scratch3/data/CovidTweets/2021_embedding/'
for filename in os.listdir(PATH):
    tweets = json.load(open(os.path.join(PATH, filename)))
    tweets = tweets['covid_vaccine_dataset']

    print(filename)
    name, _ = filename.split('.')
    _, _, _, month = name.split('_')

    sentiment_words = {'positive': [], 'negative': []};
    emotion_words = {}; moral_words = []

    for tw in tweets:
        if 'positive' in tw['keys']:
            sentiment_words['positive'] += tw['keys']['positive']
            sentiment_words_all['positive'] += tw['keys']['positive']
        if 'negative' in tw['keys']:
            sentiment_words['negative'] += tw['keys']['negative']
            sentiment_words_all['negative'] += tw['keys']['negative']
        if 'moral' in tw['keys']:
            moral_words += tw['keys']['moral']
            moral_words_all += tw['keys']['moral']

    with open('/scratch3/data/CovidTweets/top200_frequent_positive_words_{}.txt'.format(month), 'w') as fp:
        for w in Counter(sentiment_words['positive']).most_common(200):
            fp.write(str(w))
            fp.write('\n')

    with open('/scratch3/data/CovidTweets/top200_frequent_negative_words_{}.txt'.format(month), 'w') as fp:
        for w in Counter(sentiment_words['negative']).most_common(200):
            fp.write(str(w))
            fp.write('\n')

    with open('/scratch3/data/CovidTweets/top200_frequent_moral_words_{}.txt'.format(month), 'w') as fp:
        for w in Counter(moral_words).most_common(200):
            fp.write(str(w))
            fp.write('\n')

with open('/scratch3/data/CovidTweets/top200_frequent_positive_words_ALL.txt'.format(month), 'w') as fp:
    for w in Counter(sentiment_words_all['positive']).most_common(200):
        fp.write(str(w))
        fp.write('\n')

with open('/scratch3/data/CovidTweets/top200_frequent_negative_words_ALL.txt'.format(month), 'w') as fp:
    for w in Counter(sentiment_words_all['negative']).most_common(200):
        fp.write(str(w))
        fp.write('\n')

with open('/scratch3/data/CovidTweets/top200_frequent_moral_words_ALL.txt'.format(month), 'w') as fp:
    for w in Counter(moral_words_all).most_common(200):
        fp.write(str(w))
        fp.write('\n')
