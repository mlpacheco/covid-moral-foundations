import json
import argparse
from tqdm import tqdm
from nltk.tokenize import TweetTokenizer
import re
import os

def translate(label):
    label = label.lower()
    if label.startswith('care') or label.startswith('harm'):
        return 'care/harm'
    elif label.startswith('fairness') or label.startswith('cheating'):
        return 'fairness/cheating'
    elif label.startswith('loyalty') or label.startswith('betrayal') or label.startswith('ingroup'):
        return 'loyalty/betrayal'
    elif label.startswith('authority') or label.startswith('subversion'):
        return 'authority/subversion'
    elif label.startswith('purity') or label.startswith('sanctity') or label.startswith('degradation'):
        return 'purity/degradation'
    elif label.startswith('moral'):
        return 'moral'
    else:
        print(label)
        return label

def get_mfd_keywords(filename, v2=False):
    id2mf = {}; key2mf = {}; keystar2mf = {}
    step = 0
    with open(filename) as fp:
        for line in fp:
            if line.startswith('%'):
                step += 1
                continue
            elems = line.strip().split()
            if len(elems) >= 2:
                if v2 and step > 1:
                    words = tuple(elems[:-1])
                    labels = [elems[-1]]
                else:
                    words = elems[0]
                    labels = elems[1:]
                if step == 1:
                    id2mf[words] = labels[0]
                else:
                    if isinstance(words, str) and words.endswith('*'):
                        key2mf[words[:-1]] = [translate(id2mf[x]) for x in labels]
                    else:
                        key2mf[words] = [translate(id2mf[x]) for x in labels]
                    keystar2mf[words] = [translate(id2mf[x]) for x in labels]
    return id2mf, keystar2mf, key2mf


def get_sentiment_lexicon_keywords():
    positive_keys = []; negative_keys = []
    with open("/scratch3/data/CovidTweets/resources/posemo.txt") as fp:
        for line in fp:
            words = line.strip().split()
            positive_keys.append(words)
    with open("/scratch3/data/CovidTweets/resources/negemo.txt") as fp:
        for line in fp:
            words = line.strip().split()
            negative_keys.append(words)
    return positive_keys, negative_keys

def get_emotion_lexicon_keywords():
    key2emotion = {}
    with open("/scratch3/data/CovidTweets/resources/NRC-Emotion-Lexicon/NRC-Emotion-Lexicon-v0.92/NRC-Emotion-Lexicon-Wordlevel-v0.92.txt") as fp:
        for line in fp:
            word, emotion, label = line.strip().split()
            label = int(label)
            if label == 1 and emotion not in ['positive', 'negative']:
                key2emotion[word] = emotion
    return key2emotion

def build_keyword_regex(keywords):
    regex = "(?:\W|^)("
    for k in keywords:
        if k.endswith("*"):
            regex += "{}(?=\w*)".format(k[:-1])
        else:
            regex += "{}".format(k)
        regex += "|"
    regex = regex[:-1] + ")(?:\W|$)"
    return regex

def main(args):
    if args.moral:
        _, keystar2mf, key2mf = get_mfd_keywords('/scratch3/data/CovidTweets/resources/moral_foundation_dictionary.txt')
    if args.moral2:
        _, keystar2mf2, key2mf2 = get_mfd_keywords('/scratch3/data/CovidTweets/resources/mfd2.0.dic', v2=True)
    if args.sentiment:
        positive_keys, negative_keys = get_sentiment_lexicon_keywords()
    if args.emotion:
        key2emotion = get_emotion_lexicon_keywords()

    tt = TweetTokenizer()
    for filename in os.listdir(args.tweet_dir):
        if not filename.endswith(".json") and not filename.startswith("tweets"):
            continue
        print(filename)
        name, _ = filename.split('.')
        _, _, month = name.split('_')

        filename = os.path.join(args.tweet_dir, filename)
        tweets = json.load(open(filename))
        pbar = tqdm(total=len(tweets['tweets']))
        results = []
        for tw_i, tw in enumerate(tweets['tweets']):
            #print(tw.keys())
            text = tw['text']
            text_lower = text.lower()
            tweet_tokens = tt.tokenize(text_lower)
            result = {'text': text, 'text_lower': text_lower, 'tokens': tweet_tokens, 'labels': [], 'keys': {}}

            for i, tok in enumerate(tweet_tokens):
                # Find moral things
                if args.moral:
                    for moral_key in keystar2mf:
                        if (moral_key.endswith('*') and tok.startswith(moral_key[:-1])) or tok == moral_key:
                            if 'moral' not in result['keys']:
                                result['keys']['moral'] = []
                                result['keys']['moral_foundation'] = {}
                                result['labels'].append('moral')
                            labels = keystar2mf[moral_key]
                            #print(labels)
                            for label in labels:
                                if 'mf_'+label not in result['labels']:
                                    result['labels'].append("mf_"+label)
                                if label not in result['keys']['moral_foundation']:
                                    result['keys']['moral_foundation'][label] = []
                                result['keys']['moral_foundation'][label].append(moral_key)
                            result['keys']['moral'].append(moral_key)
                if args.moral2:
                    for moral_keys in keystar2mf2:
                        if (len(moral_keys) == 1 and tok == moral_keys[0]) or \
                           (len(moral_keys) == 2 and tok == moral_keys[0] and (i+1 < len(tweet_tokens)) and tweet_tokens[i+1] == moral_keys[1]) or \
                           (len(moral_keys) == 3 and tok == moral_keys[0] and (i+2 < len(tweet_tokens)) and tweet_tokens[i+1] == moral_keys[1] and tweet_tokens[i+2] == moral_keys[2]) or \
                           (len(moral_keys) == 4 and tok == moral_keys[0] and (i+3 < len(tweet_tokens)) and tweet_tokens[i+1] == moral_keys[1] and tweet_tokens[i+2] == moral_keys[2] and tweet_tokens[i+3] == moral_keys[3]):

                            if 'moral_2' not in result['keys']:
                                result['keys']['moral_2'] = []
                                result['keys']['moral_foundation_2'] = {}
                                result['labels'].append('moral_2')
                            labels = keystar2mf2[moral_keys]
                            #print(labels)
                            for label in labels:
                                if 'mf2_'+label not in result['labels']:
                                    result['labels'].append("mf2_"+label)
                                if label not in result['keys']['moral_foundation_2']:
                                    result['keys']['moral_foundation_2'][label] = []
                                result['keys']['moral_foundation_2'][label].append(" ".join(moral_keys))
                            result['keys']['moral_2'].append(" ".join(moral_keys))
                # Find sentiment things
                if args.sentiment:
                    for words in positive_keys:
                        if len(words) == 1:
                            if (words[0].endswith('*') and tok.startswith(words[0][:-1])) or tok == words[0]:
                                if 'positive' not in result['keys']:
                                    result['keys']['positive'] = []
                                    result['labels'].append('positive')
                                result['keys']['positive'].append(words[0])
                        elif len(words) == 2:
                            if ((words[0].endswith('*') and tok.startswith(words[0][:-1])) or tok == words[0]) and\
                               (i + 1 < len(tweet_tokens)) and\
                               ((words[1].endswith('*') and tweet_tokens[i+1].startswith(words[1][:-1])) or tweet_tokens[i+1] == words[1]):

                                if 'positive' not in result['keys']:
                                    result['keys']['positive'] = []
                                    result['labels'].append('positive')
                                result['keys']['positive'].append(" ".join(words))
                    for words in negative_keys:
                        if len(words) == 1:
                            if (words[0].endswith('*') and tok.startswith(words[0][:-1])) or tok == words[0]:
                                if 'negative' not in result['keys']:
                                    result['keys']['negative'] = []
                                    result['labels'].append('negative')
                                result['keys']['negative'].append(words[0])
                        elif len(words) == 2:
                            if ((words[0].endswith('*') and tok.startswith(words[0][:-1])) or tok == words[0]) and\
                               (i + 1 < len(tweet_tokens)) and\
                               ((words[1].endswith('*') and tweet_tokens[i+1].startswith(words[1][:-1])) or tweet_tokens[i+1] == words[1]):

                                if 'negative' not in result['keys']:
                                    result['keys']['negative'] = []
                                    result['labels'].append('negative')
                                result['keys']['negative'].append(" ".join(words))
                # Find emotion things
                if args.emotion:
                    for emotion_key in key2emotion:
                        if tok == emotion_key:
                            if 'emotion' not in result['keys']:
                                result['keys']['emotion'] = {}
                            label = key2emotion[emotion_key]
                            if label not in result['labels']:
                                result['labels'].append(label)
                                result['keys']['emotion'][label] = []
                            result['keys']['emotion'][label].append(emotion_key)
            results.append(result)
            pbar.update(1)

            #if tw_i == 100:
            #    break
            #break
        pbar.close()
        results_json = {'covid_vaccine_dataset': results}
        print("Writing month {}".format(month))
        with open("/scratch3/data/CovidTweets/2021_embedding/embedding_dataset_2021_{}.json".format(month), "w") as fp:
            json.dump(results_json, fp)
        #break
        #exit()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--moral', default=False, action='store_true')
    parser.add_argument('--moral2', default=False, action='store_true')
    parser.add_argument('--sentiment', default=False, action='store_true')
    parser.add_argument('--tweet_dir', type=str, required=True)
    parser.add_argument('--emotion', default=False, action='store_true')
    
    args = parser.parse_args()
    main(args)
