import json
import argparse
from collections import Counter
import wikipedia

def main(args):
    entity2lexform = json.load(open(args.entity2lexform))

    all_entities = []
    for ent in entity2lexform:
        lexform = entity2lexform[ent].lower()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--covid_path', type=str, required=True)
    parser.add_argument('--entity2lexform', type=str, required=True)
    args = parser.parse_args()
    main(args)

