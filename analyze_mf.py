import numpy as np
import random
import argparse
from gensim.corpora.dictionary import Dictionary
from gensim.utils import tokenize
from gensim.models.ldamodel import LdaModel
from gensim.parsing.preprocessing import remove_stopwords
import json
from collections import Counter
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import seaborn as sn
import matplotlib.pyplot as plt


def normalize_mf(mf):
    if mf == 'fairness/cheating':
        return 'fair/cheat'
    elif mf == 'liberty/oppression':
        return 'lib/opp'
    elif mf == 'authority/subversion':
        return 'auth/subv'
    elif mf == 'sanctity/degradation':
        return 'pure/degrade'
    elif mf == 'purity/degradation':
        return 'pure/degrade'
    elif mf == 'loyalty/betrayal':
        return 'loyal/betray'
    else:
        return mf

def analyze_mf(args, theme_file, mf):
    id2theme = {}
    with open('drail_programs/data/{}.txt'.format(theme_file)) as fp:
        for line in fp:
            _id, _theme = line.strip().split('\t')
            id2theme[_id] = _theme
    id2stance = {}
    with open('drail_programs/data/has_stance.txt') as fp:
        for line in fp:
            _id, _stance = line.strip().split('\t')
            id2stance[_id] = int(_stance)
    id2mf = {}
    with open('drail_programs/data/has_mf.txt') as fp:
        for line in fp:
            _id, _mf = line.strip().split('\t')
            if _mf == mf:
                id2mf[_id] = _mf

    id2ent = {}
    entity2lexform = json.load(open('drail_programs/data/entity2lexform.json'))
    with open('drail_programs/data/has_role.txt') as fp:
        for line in fp:
            _id, ent, role = line.strip().split('\t')
            ent = entity2lexform[ent]
            if _id not in id2ent:
                id2ent[_id] = {}
            if ent not in id2ent[_id]:
                id2ent[_id][ent] = {}
            id2ent[_id][ent]['role'] = role
    with open('drail_programs/data/has_sentiment.txt') as fp:
        for line in fp:
            _id, ent, sent = line.strip().split('\t')
            ent = entity2lexform[ent]
            if _id not in id2ent:
                id2ent[_id] = {}
            if ent not in id2ent[_id]:
                id2ent[_id][ent] = {}
            id2ent[_id][ent]['sent'] = sent

    reasons = []
    for _id in id2mf:
        reasons.append(id2theme[_id])
    print(Counter(reasons))
    for reason in set(reasons):
        stances = []; frames = []
        for _id in id2mf:
            if id2theme[_id] == reason and _id in id2stance:
                stances.append(id2stance[_id])
                if _id in id2ent:
                    for ent in id2ent[_id]:
                        frames.append((ent, id2ent[_id][ent]['role'], id2ent[_id][ent]['sent'], id2stance[_id]))
        print('#################')
        print(reason)
        print('------')
        print('stances', Counter(stances))
        print('frames', Counter(frames))

def main(args):

    analyze_mf(args, 'has_theme', 'fairness/cheating')

if __name__ == "__main__":
    np.random.seed(1534)
    random.seed(1534)

    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    main(args)
