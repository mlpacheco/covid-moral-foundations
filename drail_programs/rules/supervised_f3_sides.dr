// Entities
entity: "Tweet", arguments: ["tweetId"::ArgumentType.UniqueString];
entity: "Entity", arguments: ["entityId"::ArgumentType.UniqueString];
entity: "MoralFoundation", arguments: ["mfId"::ArgumentType.UniqueString];
entity: "Role", arguments: ["roleId"::ArgumentType.UniqueString];
entity: "Sentiment", arguments: ["sentimentId"::ArgumentType.UniqueString];
entity: "Morality", arguments: ["moralityId"::ArgumentType.UniqueString];
entity: "Stance", arguments: ["stanceId"::ArgumentType.UniqueString];
entity: "Instance", arguments: ["instanceId"::ArgumentType.UniqueString];
entity: "Argument", arguments: ["argumentId"::ArgumentType.UniqueString];

// Relations
predicate: "IsTweet", arguments: [Tweet];
predicate: "HasEntity", arguments: [Tweet, Entity];
predicate: "HasMoralFoundation", arguments: [Tweet, MoralFoundation];
predicate: "HasMorality", arguments: [Tweet, Morality];
predicate: "HasRole", arguments: [Tweet, Entity, Role];
predicate: "HasSentiment", arguments: [Tweet, Entity, Sentiment];
predicate: "HasStance", arguments: [Tweet, Stance];
predicate: "InInstance", arguments: [Tweet, Instance];
predicate: "MentionsArgument", arguments: [Tweet, Argument];


// Labels
label: "RoleLabel", classes: 2, type: LabelType.Multiclass;
label: "SentimentLabel", classes: 2, type: LabelType.Multiclass;
label: "StanceLabel", classes: 3, type: LabelType.Multiclass;
label: "MoralFoundationLabel", classes: 7, type: LabelType.Multiclass;
label: "MoralityLabel", classes: 2, type: LabelType.Multiclass;

load: "IsTweet", file: "is_tweet.txt";
load: "HasEntity", file: "has_entity.txt";
load: "HasMoralFoundation", file: "has_mf.txt";
load: "HasMorality", file: "has_morality.txt";
load: "HasRole", file: "has_role.txt";
load: "HasSentiment", file: "has_sentiment.txt";
load: "HasStance", file: "has_stance.txt";
load: "InInstance", file: "in_instance_lexform.txt";
load: "MentionsArgument", file: "has_theme.txt";

load: "RoleLabel", file: "role_label.txt";
load: "MoralityLabel", file: "morality_label.txt";
load: "MoralFoundationLabel", file: "mf_label.txt";
load: "SentimentLabel", file: "sentiment_label.txt";
load: "StanceLabel", file: "stance_label.txt";

// Feature classes
femodule: "mf_ft";
feclass: "MF_ft";

ruleset {

  rule: IsTweet(T) & InInstance(T, I) & HasStance(T, S) & MentionsArgument(T, A) => HasStance(T, X^StanceLabel?),
  lambda: 1.0,
  network: "config.json",
  fefunctions: [
    input("tweet_bert"),
    vector("argument_1hot")
  ];

} groupby: InInstance.2;
