import json
from drail.features.feature_extractor import FeatureExtractor

class MF_ft(FeatureExtractor):

    def __init__(self, tweets_f, entities_f, debug=False):
        self.tweets_f = tweets_f
        self.entities_f = entities_f

    def build(self):
        self.tweets = json.load(open(self.tweets_f))
        self.entities = json.load(open(self.entities_f))

        self.morality2idx = {
                'moral': 0,
                'non-moral': 1
        }

        self.mf2dix = {
            'authority/subversion': 0,
            'care/harm': 1,
            'fairness/cheating': 2,
            'liberty/oppression': 3,
            'loyalty/betrayal': 4,
            'none': 5,
            'purity/degradation': 6
        }

        self.sent2idx = {
            'sentiment-neg': 0,
            'sentiment-pos': 1
        }

        self.role2idx = {
            'actor': 0,
            'target': 1
        }

        self.argument2idx = {
            'GovDistrust': 0,
            'GovTrust': 1,
            'VaccineDanger': 2,
            'VaccineSafe': 3,
            'CovidFake': 4,
            'CovidReal': 5,
            'VaccineOppression': 6,
            'VaccineNotOppression': 7,
            'BigPharmaAnti': 8,
            'BigPharmaPro': 9,
            'NaturalImmunityPro': 10,
            'NaturalImmunityAnti': 11,
            'vaccineAgainstReligion': 12,
            'vaccineNotAgainstRelition': 13,
            'vaccineDoesntWork': 14,
            'vaccineWorks': 15,
            'VaccineTested': 16,
            'VaccineNotTested': 17,
            'VaccineExperimentsOnDogs': 18,
            'BillGatesMicroChip': 19,
            'VaccinesContainAbortedFetalTissue': 20,
            'VaccinesDoNotContainFetalTissue': 21,
            'VaccinesMakeYouSterile': 22,
            'VaccinesDoNotMakeYouSterile': 23
        }

    def tweet_bert(self, rule_grd):
        tweet_id = rule_grd.get_body_predicates('IsTweet')[0]['arguments'][0]
        encoded_inputs = self.tweets[tweet_id]
        return (encoded_inputs['input_ids'], encoded_inputs['token_type_ids'], encoded_inputs['attention_mask'])

    def tweet_entity_bert(self, rule_grd):
        entity_id = rule_grd.get_body_predicates('HasEntity')[0]['arguments'][1]
        encoded_inputs = self.entities[entity_id]
        return (encoded_inputs['input_ids'], encoded_inputs['token_type_ids'], encoded_inputs['attention_mask'])

    def role_1hot(self, rule_grd):
        role_id = rule_grd.get_body_predicates('HasRole')[-1]['arguments'][-1]
        vect = [0.0] * len(self.role2idx)
        vect[self.role2idx[role_id]] = 1.0
        return vect

    def sentiment_1hot(self, rule_grd):
        sent_id = rule_grd.get_body_predicates('HasSentiment')[-1]['arguments'][-1]
        vect = [0.0] * len(self.sent2idx)
        vect[self.sent2idx[sent_id]] = 1.0
        return vect

    def stance_1hot(self, rule_grd):
        stance_id = rule_grd.get_body_predicates('HasStance')[-1]['arguments'][-1]
        vect = [0.0, 0.0]
        vect[int(stance_id)] = 1.0
        return vect

    def argument_1hot(self, rule_grd):
        arg_id = rule_grd.get_body_predicates('MentionsArgument')[-1]['arguments'][-1]
        vect = [0.0] * len(self.argument2idx)
        vect[self.argument2idx[arg_id]] = 1.0
        return vect

    def extract_multiclass_head(self, rule_grd):
        head = rule_grd.get_head_predicate()
        label = head['arguments'][-1]
        if head['name'] == 'HasRole':
            return self.role2idx[label]
        elif head['name'] == 'HasSentiment':
            return self.sent2idx[label]
        elif head['name'] == 'HasMorality':
            return self.morality2idx[label]
        elif head['name'] == 'HasMoralFoundation':
            return self.mf2dix[label]
        else:
            return int(label)
