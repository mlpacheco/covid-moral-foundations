import argparse
import torch
import json
from torch.utils import data
import os
from transformers import AutoTokenizer
from tqdm.auto import tqdm
import random
import pandas as pd
from models.embeddings import Embedding
from run_mf_classifier import prepare_political_dataset, prepare_mftc_dataset
from sklearn.metrics import f1_score, confusion_matrix, classification_report

class TextKeywordConceptDataset(data.Dataset):

    def __init__(self, input_ids, token_ids, attention_mask,
                 concepts, keywords, negative_keywords):
        self.input_ids = input_ids
        self.token_ids = token_ids
        self.attention_mask = attention_mask
        self.keywords = keywords
        self.negative_keywords = negative_keywords
        self.concepts = concepts

    def __len__(self):
        return len(self.input_ids)

    def __getitem__(self, index):
        input_ids_i = self.input_ids[index]
        token_ids_i = self.token_ids[index]
        attention_mask_i = self.attention_mask[index]
        concept_i = self.concepts[index]
        keyword_i = self.keywords[index]
        negative_keywords_i = self.negative_keywords[index]

        return input_ids_i, token_ids_i, attention_mask_i,\
               concept_i, keyword_i, negative_keywords_i

class TextDataset(data.Dataset):

    def __init__(self, input_ids, token_ids, attention_mask, labels):
        self.input_ids = input_ids
        self.token_ids = token_ids
        self.attention_mask = attention_mask
        self.labels = labels

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, index):
        input_ids_i = self.input_ids[index]
        token_ids_i = self.token_ids[index]
        attention_mask_i = self.attention_mask[index]
        label = self.labels[index]

        return input_ids_i, token_ids_i, attention_mask_i, label

def negative_sampling(keyword, concept, concept_keys):
    cands = []
    for _concept in concept_keys:
        if _concept != concept:
            cands += concept_keys[_concept]
    random.shuffle(cands)
    return cands[:5]


def prepare_tweets(args):
    max_dim = 0; all_dims = []
    months = [1, 4, 6, 7, 8, 9]
    # Using BERT-mini
    input_ids = []; token_type_ids = []; attention_mask = []
    keys = []; negative_keys = []; concepts = []
    concept_keys = {}; key2idx = {}; concept2idx = {}
    concept_idx = 0; key_idx = 0

    tokenizer = AutoTokenizer.from_pretrained('google/bert_uncased_L-2_H-128_A-2')
    for m in months:
        all_text = [];
        with open(os.path.join(args.tweets_path, "embedding_dataset_2021_m{}.json".format(m))) as fp:
            tweets = json.load(fp)
            pbar = tqdm(total=len(tweets['covid_vaccine_dataset']), desc='month {}'.format(m))
            for tw in tweets['covid_vaccine_dataset']:
                if 'moral_foundation_2' in tw['keys']:
                    for concept in tw['keys']['moral_foundation_2']:
                        for keyw in tw['keys']['moral_foundation_2'][concept]:
                            text = tw['text_lower']
                            if concept not in concept2idx:
                                concept2idx[concept] = concept_idx
                                concept_keys[concept_idx] = set()
                                concept_idx += 1
                            if keyw not in key2idx:
                                key2idx[keyw] = key_idx
                                key_idx += 1

                            concept_keys[concept2idx[concept]].add(key2idx[keyw])
                            keys.append(key2idx[keyw])
                            concepts.append(concept2idx[concept])
                            text = text.replace(keyw, '[MASK]')
                            all_text.append(text)
                pbar.update(1)
        pbar.close()
        #print(concept_keys)
        #exit()

        encoded_inputs = tokenizer(all_text, padding='max_length', truncation=True, return_tensors='pt', max_length=80)

        #for mask in encoded_inputs['attention_mask']:
        #    all_dims.append(mask.sum().item())

        input_ids.append(encoded_inputs['input_ids'])
        token_type_ids.append(encoded_inputs['token_type_ids'])
        attention_mask.append(encoded_inputs['attention_mask'])
        max_dim = max(max_dim, encoded_inputs['input_ids'].shape[1])

    #print(type(all_dims), len(all_dims), type(all_dims[0]))
    #s = pd.Series(all_dims)
    #print(s.describe())
    #exit()

    input_ids = torch.cat(input_ids, dim=0).cuda()
    token_type_ids = torch.cat(token_type_ids, dim=0).cuda()
    attention_mask = torch.cat(attention_mask, dim=0).cuda()

    pbar = tqdm(total=len(keys), desc='negative sampling')
    for keyw, cncpt in zip(keys, concepts):
        negative_keys.append(negative_sampling(keyw, cncpt, concept_keys))
        pbar.update(1)
    pbar.close()

    keys = torch.LongTensor(keys).cuda()
    concepts = torch.LongTensor(concepts).cuda()
    negative_keys = torch.LongTensor(negative_keys).cuda()

    print(input_ids.shape, keys.shape, negative_keys.shape)
    #exit()
    print('saving tensors....')
    torch.save(input_ids, os.path.join(args.tensor_path, 'input_ids.pt'))
    torch.save(token_type_ids, os.path.join(args.tensor_path, 'token_type_ids.pt'))
    torch.save(attention_mask, os.path.join(args.tensor_path, 'attention_mask.pt'))
    torch.save(keys, os.path.join(args.tensor_path, 'keywords.pt'))
    torch.save(concepts, os.path.join(args.tensor_path, 'concepts.pt'))
    torch.save(negative_keys, os.path.join(args.tensor_path, 'negative_keys.pt'))

    with open(os.path.join(args.tensor_path, 'concept2idx.json'), 'w') as fp:
        json.dump(concept2idx, fp)
    print('done.')

def load_tensors(args):
    input_ids = torch.load(os.path.join(args.tensor_path, 'input_ids.pt')).cuda()
    token_type_ids = torch.load(os.path.join(args.tensor_path, 'token_type_ids.pt')).cuda()
    attention_mask = torch.load(os.path.join(args.tensor_path, 'attention_mask.pt')).cuda()
    keys = torch.load(os.path.join(args.tensor_path, 'keywords.pt')).cuda()
    concepts = torch.load(os.path.join(args.tensor_path, 'concepts.pt')).cuda()
    negative_keys = torch.load(os.path.join(args.tensor_path, 'negative_keys.pt')).cuda()

    concept2idx = json.load(open(os.path.join(args.tensor_path, 'concept2idx.json')))

    return input_ids, token_type_ids, attention_mask,\
           keys, negative_keys, concepts, concept2idx

def predict(dev_dataloader, model, concept_labels):
    model.eval()
    pbar = tqdm(total=len(dev_dataloader), desc='checking dev performance')

    y_pred_all = []; y_gold_all = []
    pbar = tqdm(range(len(dev_dataloader)))
    for input_ids, token_ids, attention_mask, labels in dev_dataloader:
        similarities = model.predict(input_ids, token_ids, attention_mask)
        _, y_pred = torch.max(similarities, 1)
        y_pred_all += list(y_pred.cpu())
        y_gold_all += list(labels.cpu())

        pbar.update(1)
    pbar.close()
    wf1 = f1_score(y_gold_all, y_pred_all, average='weighted')
    mf1 = f1_score(y_gold_all, y_pred_all, average='macro')
    print("DEV - weighted F1: {0}, macro F1: {1}".format(wf1, mf1))
    print(classification_report(y_gold_all, y_pred_all, target_names=concept_labels, digits=4))
    return wf1, mf1

def train(train_dataloader, dev_dataloader, model, concept_labels):
    dev_wf1, dev_mf1 = predict(dev_dataloader, model, concept_labels)
    best_f1 = 0

    optimizer = torch.optim.AdamW(model.parameters(), lr=2e-5)
    best_f1 = 0; patience = 1
    while patience > 0:
        train_loss = 0; train_batches = 0
        pbar = tqdm(total=len(train_dataloader), desc='train epoch')
        model.train()
        for input_ids, token_ids, attention_mask, concepts, keywords, negative_keywords in train_dataloader:
            model.zero_grad()
            loss_text_key, loss_key_concept = model(input_ids, token_ids, attention_mask,
                                                    concepts, keywords, negative_keywords)

            accum_loss = loss_text_key + loss_key_concept
            train_loss += accum_loss.item()
            train_batches += 1
            accum_loss.backward()
            optimizer.step()

            pbar.update(1)
        pbar.close()
        train_loss = train_loss / train_batches
        print("Train loss: {0}, weighted F1: {1}, macro F1: {2}".format(train_loss, wf1, mf1))

        dev_wf1, dev_mf1 = predict(dev_dataloader, model, concept_labels)
        if dev_mf1 > best_f1:
            best_f1 = wf1_dev
            patience = 1
            torch.save(model.state_dict(), "/scratch1/pachecog/covid/MF2_embed_model.pt")
        else:
            patience -= 1

def main(args):
    if args.tweets_path:
        prepare_tweets(args)
        exit()

    input_ids, token_type_ids, attention_mask,\
        keys, negative_keys, concepts, concept2idx = load_tensors(args)
    idx2concept = {v: k for k, v in concept2idx.items()}
    num_keywords = keys.max().item() + 1
    num_concepts = concepts.max().item() + 1
    concept_labels = [idx2concept[i] for i in range(0, num_concepts)]

    print("keywords:", num_keywords, "concepts:", num_concepts)

    train_dataset = TextKeywordConceptDataset(
        input_ids, token_type_ids, attention_mask,
        concepts, keys, negative_keys
    )

    train_dataloader = data.DataLoader(train_dataset, batch_size=32, shuffle=True)

    # Use annotated data as dev set
    input_ids_dev = []; token_ids_dev = []; attention_mask_dev = []
    mf_labels_dev = []

    _input_ids, _token_ids, _attention_mask,\
        _, _mf_labels = prepare_political_dataset(args.political_path, args)
    input_ids_dev += _input_ids
    token_ids_dev += _token_ids
    attention_mask_dev += _attention_mask
    mf_labels_dev += _mf_labels

    _input_ids, _token_ids, _attention_mask,\
        _, _mf_labels = prepare_mftc_dataset(args.mftc_path, args)
    input_ids_dev += _input_ids
    token_ids_dev += _token_ids
    attention_mask_dev += _attention_mask
    mf_labels_dev += _mf_labels
    mf_labels_dev = [concept2idx[c] for c in mf_labels_dev]

    input_ids_dev = torch.LongTensor(input_ids_dev).cuda()
    token_ids_dev = torch.LongTensor(token_ids_dev).cuda()
    attention_mask_dev = torch.LongTensor(attention_mask_dev).cuda()
    mf_labels_dev = torch.LongTensor(mf_labels_dev).cuda()

    dev_dataset = TextDataset(input_ids_dev, token_ids_dev, attention_mask_dev, mf_labels_dev)
    dev_dataloader = data.DataLoader(dev_dataset, batch_size=32)

    print("train:", len(train_dataloader), "dev:", len(dev_dataloader))

    model = Embedding(n_keywords=num_keywords, n_concepts=num_concepts,
                      bert_model_name='google/bert_uncased_L-2_H-128_A-2')
    model.cuda()

    train(train_dataloader, dev_dataloader, model, concept_labels)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--tweets_path', type=str, default=None)
    parser.add_argument('--tensor_path', type=str, required=True)
    parser.add_argument('--political_path', type=str, default=None)
    parser.add_argument('--mftc_path', type=str, default=None)
    parser.add_argument('--ismoral', default=False)
    #parser.add_argument('--moral_sentiment', default=None, type=str, help='path to moral sentiment DS')
    args = parser.parse_args()
    main(args)
