import json
import logging.config
import argparse
import numpy as np
from sklearn.metrics import *
import torch
import random
from pathlib import Path
import os
from drail import database
from drail.learn.local_learner import LocalLearner
from termcolor import colored

def process_head(head):
    name, args = head.split('(')
    args = args[:-1]
    args = args.split(',')
    return name, args

def seed_examples(args, dataset, seed_tweet_ids):
    for predicate in dataset:
        with open(os.path.join(args.dir, predicate + ".txt")) as fp:
            for line in fp:
                elems = line.strip().split('\t')
                if int(elems[0]) in seed_tweet_ids:
                    dataset[predicate].add(tuple(elems))

def seed_inference(args, learner, seed_tweet_ids):
    with open(os.path.join(args.dir, 'has_mf.txt')) as fp:
        for line in fp:
            elems = line.strip().split('\t')
            if elems[0] in seed_tweet_ids:
                learner.define_hardconstr('InInstance("{0}", I) & IsTweet("{0}") & HasMorality("{0}", M) => HasMoralFoundation("{0}", "{1}")^?'.format(elems[0], elems[1]))
    with open(os.path.join(args.dir, 'has_morality.txt')) as fp:
        for line in fp:
            elems = line.strip().split('\t')
            if elems[0] in seed_tweet_ids:
                learner.define_hardconstr('InInstance("{0}", I) & IsTweet("{0}") & HasMorality("{0}", M) => HasMorality("{0}", "{1}")^?'.format(elems[0], elems[1]))
    with open(os.path.join(args.dir, 'has_stance.txt')) as fp:
        for line in fp:
            elems = line.strip().split('\t')
            if elems[0] in seed_tweet_ids:
                learner.define_hardconstr('InInstance("{0}", I) & IsTweet("{0}") & HasStance("{0}", S) => HasStance("{0}", "{1}")^?'.format(elems[0], elems[1]))
    with open(os.path.join(args.dir, 'has_role.txt')) as fp:
        for line in fp:
            elems = line.strip().split('\t')
            if elems[0] in seed_tweet_ids:
                learner.define_hardconstr('InInstance("{0}", I) & IsTweet("{0}") & HasEntity("{0}", "{1}") => HasRole("{0}", "{1}", "{2}")^?'.format(elems[0], elems[1], elems[2]))
    #with open(os.path.join(args.dir, 'has_sentiment.txt')) as fp:
    #    for line in fp:
    #        elems = line.strip().split('\t')
    #        if elems[0] in seed_tweet_ids:
    #           learner.define_hardconstr('InInstance("{0}", I) & IsTweet("{0}") & HasEntity("{0}", "{1}") => HasSentiment("{0}", "{1}", "{2}")^?'.format(elems[0], elems[1], elems[2]))


def expectation_step(learner, db, args, dev_tweets, test_tweets, seed_train):
    dataset = {
        'has_mf': set(),
        'has_morality': set(),
        'has_role': set(),
        'has_sentiment': set(),
        'has_stance': set()
    }

    learner.init_models()

    learner.extract_instances(db,
            extract_train=True, test_filters=[("IsTweet", "isTrain", 1)])
    res, heads = learner.predict(db, fold_filters=[("IsTweet", "isTrain", 1)], fold='train', get_predicates=True)
    # Now write the updated files
    for h in heads:
        name, argums = process_head(h)
        tw_id = argums[0]
        if tw_id not in seed_train:
            if name == 'HasMoralFoundation':
                dataset['has_mf'].add(tuple(argums))
            elif name == 'HasMorality':
                dataset['has_morality'].add(tuple(argums))
            elif name == 'HasRole':
                dataset['has_role'].add(tuple(argums))
            elif name == 'HasSentiment':
                dataset['has_sentiment'].add(tuple(argums))
            elif name == 'HasStance':
                dataset['has_stance'].add(tuple(argums))

    seed_examples(args, dataset, dev_tweets)
    seed_examples(args, dataset, test_tweets)
    seed_examples(args, dataset, seed_train)

    for predicate in dataset:
        with open(os.path.join(args.dir_dyn, predicate + '.txt'), 'w') as fp:
            for tup in dataset[predicate]:
                fp.write('\t'.join(tup))
                fp.write('\n')

    learner.reset_metrics()
    db = learner.create_dataset(args.dir_dyn)
    return db

def maximization_step(learner, db, optimizer):
    learner.train(db,
                  train_filters=[("IsTweet", "isTrain", 1)],
                  dev_filters=[("IsTweet", "isDev", 1)],
                  test_filters=[("IsTweet", "isTest", 1)],
                  optimizer=optimizer)

def evaluate_dev(learner, db, filter_name, fold_name):
    learner.init_models()
    learner.extract_instances(db,
            extract_dev=True, test_filters=[("IsTweet", filter_name, 1)])
    res, heads = learner.predict(db, fold_filters=[("IsTweet", filter_name, 1)], fold=fold_name, get_predicates=True)
    weighted_f1 = 0; n_count = 1
    for pred in set(['HasRole', 'HasMoralFoundation', 'HasSentiment', 'HasStance', 'HasMorality']):
        #if pred in set(['HasMoralFoundation']):
            y_gold = res.metrics[pred]['gold_data']
            y_pred = res.metrics[pred]['pred_data']
            weighted_f1 += f1_score(y_gold, y_pred, average='weighted')
            n_count += 1
            #print(classification_report(y_gold, y_pred, digits=4))
    learner.reset_metrics()
    return weighted_f1/n_count

def evaluate_test(learner, avg_metrics, db):
    learner.init_models()
    learner.extract_instances(db,
            extract_test=True, test_filters=[("IsTweet", "isTest", 1)])
    res, heads = learner.predict(db, fold_filters=[("IsTweet", "isTest", 1)], fold='test', get_predicates=True)
    for pred in res.metrics:
        if pred in set(['HasRole', 'HasMoralFoundation', 'HasSentiment', 'HasStance', 'HasMorality']):
            y_gold = res.metrics[pred]['gold_data']
            y_pred = res.metrics[pred]['pred_data']
            if pred not in avg_metrics:
                avg_metrics[pred] = {}
                avg_metrics[pred]['gold'] = y_gold
                avg_metrics[pred]['pred'] = y_pred
            else:
                avg_metrics[pred]['gold'].extend(y_gold)
                avg_metrics[pred]['pred'].extend(y_pred)

            #logger.info('\n'+ pred + ':\n' + classification_report(y_gold, y_pred, digits=4))
    learner.reset_metrics()

def main(args):
    folds = json.load(open(args.folds))

    optimizer = "AdamW"
    if args.gpu_index:
        torch.cuda.set_device(args.gpu_index)

    avg_metrics = {}
    for i in folds:
        if int(i) < args.start_fold:
            continue
        if not args.single_fold:
            test_tweets = folds[i]
            train_tweets = []
            for k in folds:
                if k != i:
                    train_tweets += folds[k]
            dev_split = int(len(train_tweets) * 0.8)
            dev_tweets = train_tweets[dev_split:]
            train_tweets = train_tweets[:dev_split]

        elif not args.do_train:
            test_tweets = []
            for k in folds:
                test_tweets += folds[k]
            # Adding this here so code doesn't break, but it is meaningless
            train_tweets = test_tweets[:1]
            dev_tweets = test_tweets[:1]
        else:
            print('If you are training you need to use K fold cross-val')
            exit()

        seed_train = train_tweets[:int(len(train_tweets) * args.amount_train)]

        # re-initialize learner (doing this so seeding hard constraints do not live accross folds)
        learner = LocalLearner()
        learner.compile_rules(args.rules)
        db = learner.create_dataset(args.dir_dyn)
        # seed behavior for inference
        seed_inference(args, learner, seed_train)

        torch.cuda.empty_cache()
        curr_savedir = os.path.join(args.savedir, "f{}".format(i))
        Path(curr_savedir).mkdir(parents=True, exist_ok=True)

        db.add_filters(filters=[
            ("IsTweet", "isDummy", "tweetId_1", train_tweets[0:1]),
            ("IsTweet", "isTrain", "tweetId_1", train_tweets),
            ("IsTweet", "isDev", "tweetId_1", dev_tweets),
            ("IsTweet", "isTest", "tweetId_1", test_tweets)
        ])

        print(len(train_tweets), len(dev_tweets), len(test_tweets))

        learner.build_feature_extractors(db,
                tweets_f=args.tweet2bert,
                entities_f=args.entity2bert,
                femodule_path=args.fe_path,
                filters=[("IsTweet", "isDummy", 1)])

        learner.set_savedir(curr_savedir)
        learner.build_models(db, args.config, netmodules_path=args.ne_path)

        best_f1_dev = evaluate_dev(learner, db, 'isDev', 'dev')
        best_f1_test = evaluate_dev(learner, db, 'isTest', 'test')
        term = "Dev: {}, Test: {}".format(best_f1_dev, best_f1_test)
        print(colored(term, 'red'))

        PAT = 2
        patience = PAT

        if args.do_train:
            n_epochs = 0
            while (patience > 0):

                db = expectation_step(learner, db, args, dev_tweets, test_tweets, seed_train)
                db.add_filters(filters=[
                    ("IsTweet", "isDummy", "tweetId_1", train_tweets[0:1]),
                    ("IsTweet", "isTrain", "tweetId_1", train_tweets),
                    ("IsTweet", "isDev", "tweetId_1", dev_tweets),
                    ("IsTweet", "isTest", "tweetId_1", test_tweets)
                ])

                maximization_step(learner, db, optimizer)
                f1_dev = evaluate_dev(learner, db, 'isDev', 'dev')
                f1_test = evaluate_dev(learner, db, 'isTest', 'test')

                if f1_dev > best_f1_dev:
                    best_f1_dev = f1_dev
                    best_f1_test = best_f1_test
                    patience = PAT
                else:
                    patience -= 1

                n_epochs += 1
                term = "Epoch: {}, Dev: {}, Test {}".format(n_epochs, best_f1_dev, best_f1_test)
                print(colored(term, 'red'))

        evaluate_test(learner, avg_metrics, db)

    # Final results
    for pred in avg_metrics:
        if pred == 'HasStance':
            logger.info(pred + ':\n' + classification_report(avg_metrics[pred]['gold'], avg_metrics[pred]['pred'], digits=4))
        else:
            logger.info(pred + ':\n' + classification_report(avg_metrics[pred]['gold'], avg_metrics[pred]['pred'], digits=4))

if __name__ == "__main__":
    # seed and cuda
    torch.manual_seed(1534)
    np.random.seed(1534)
    random.seed(1534)

    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--gpu', help='gpu index', dest='gpu_index', type=int, default=None)
    parser.add_argument('-d', '--dir', help='directory', dest='dir', type=str, required=True)
    parser.add_argument('--dir_dyn', help='dynamic directory', dest='dir_dyn', type=str, required=True)
    parser.add_argument('-r', '--rule', help='rule file', dest='rules', type=str, required=True)
    parser.add_argument('-c', '--config', help='config file', dest='config', type=str, required=True)
    parser.add_argument('--savedir', help='save directory', dest='savedir', type=str, required=True)
    parser.add_argument('--folds', help='folds file', dest='folds', type=str, required=True)
    parser.add_argument('--debug', help='debug mode', dest='debug', default=False, action='store_true')
    parser.add_argument('--tweet2bert', type=str, required=True)
    parser.add_argument('--entity2bert', type=str, required=True)
    parser.add_argument('--fe_path', type=str, required=True)
    parser.add_argument('--ne_path', type=str, required=True)
    parser.add_argument('--log', type=str, required=True)
    parser.add_argument('--single_fold', default=False, action='store_true')
    parser.add_argument('--do_train', default=False, action='store_true')
    parser.add_argument('--amount_train', type=float, required=True)
    parser.add_argument('--start_fold', type=int, default=0)
    args = parser.parse_args()

    logger = logging.getLogger()
    logging.config.dictConfig(json.load(open(args.log)))
    cmd = "rm -rf {0}".format(args.dir_dyn)
    print(cmd)
    os.system(cmd)
    cmd = "cp -r {0} {1}".format(args.dir, args.dir_dyn)
    print(cmd)
    os.system(cmd)

    main(args)

