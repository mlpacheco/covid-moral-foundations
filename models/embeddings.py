import torch
import torch.nn.functional as F
from transformers import AutoConfig, AutoModel, BertModel

class Embedding(torch.nn.Module):

    def __init__(self, n_keywords, n_concepts, bert_model_name):
        super(Embedding, self).__init__()
        # text embedding
        bert_config = AutoConfig.from_pretrained(bert_model_name)
        self.bert_model = AutoModel.from_pretrained(bert_model_name)
        self.dropout = torch.nn.Dropout(bert_config.hidden_dropout_prob)

        # keyword and class embedding
        self.keyword_embedding = torch.nn.Parameter(
                torch.zeros(n_keywords, bert_config.hidden_size))
        torch.nn.init.uniform_(tensor=self.keyword_embedding)
        self.n_keywords = n_keywords

        self.concept_embedding = torch.nn.Parameter(
                torch.zeros(n_concepts, bert_config.hidden_size))
        torch.nn.init.uniform_(tensor=self.concept_embedding)
        self.n_concepts = n_concepts

        self.concepts = torch.LongTensor(range(0, self.n_concepts))
        self.keywords = torch.LongTensor(range(0, self.n_keywords))

        self.cross_ent = torch.nn.CrossEntropyLoss()

    def forward(self, input_ids, token_ids, attention_mask, concepts, keys, negative_keys):
        outputs = self.bert_model(input_ids, attention_mask=attention_mask,
                                 token_type_ids=token_ids,
                                 position_ids=None, head_mask=None)
        pooled_output = outputs[1]
        pooled_output = self.dropout(pooled_output)

        # concatenate positive example and negative examples, pos example always in position 0
        # therefore, targets for crossent loss will always be 0
        all_keys = torch.cat([keys.view(-1, 1), negative_keys], dim=1)
        targets = torch.zeros(all_keys.shape[0]).long().cuda()

        # get keyword embeddings and calculate all similarities
        keyword_embeds = self.keyword_embedding[self.keywords].view(-1, self.n_keywords)
        sim_text_keys = torch.mm(pooled_output, keyword_embeds)
        # select only the indices we care about (pos and neg examples)
        sim_text_keys = sim_text_keys.gather(1, all_keys)

        loss_text_key = self.cross_ent(sim_text_keys, targets)

        obs_concept_embeds = self.concept_embedding[concepts]
        sim_concept_keys = torch.mm(obs_concept_embeds, keyword_embeds)
        sim_concept_keys = sim_concept_keys.gather(1, all_keys)

        loss_key_concept = self.cross_ent(sim_concept_keys, targets)

        return loss_text_key, loss_key_concept

    def predict(self, input_ids, token_ids, attention_mask):
        concept_embeds = self.concept_embedding[self.concepts].view(-1, self.n_concepts)

        outputs = self.bert_model(input_ids, attention_mask=attention_mask,
                                 token_type_ids=token_ids,
                                 position_ids=None, head_mask=None)
        pooled_output = outputs[1]

        # return (batch x n_concepts)
        similarities = torch.mm(pooled_output, concept_embeds)
        similarities = F.softmax(similarities, dim=1)
        return similarities
