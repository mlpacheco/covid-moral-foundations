import torch
import numpy as np
import torch.nn.functional as F
from transformers import AutoConfig, AutoModel, BertModel
from torch.autograd import Function


class RevGradFun(Function):
    @staticmethod
    def forward(ctx, input_, alpha_):
        ctx.save_for_backward(input_, alpha_)
        output = input_
        return output

    @staticmethod
    def backward(ctx, grad_output):  # pragma: no cover
        grad_input = None
        _, alpha_ = ctx.saved_tensors
        if ctx.needs_input_grad[0]:
            grad_input = -grad_output * alpha_
        return grad_input, None

class RevGrad(torch.nn.Module):
    def __init__(self, alpha=1., *args, **kwargs):
        """
        A gradient reversal layer.
        This layer has no parameters, and simply reverses the gradient
        in the backward pass.
        """
        super().__init__(*args, **kwargs)

        self._alpha = torch.tensor(alpha, requires_grad=False).cuda()

    def forward(self, input_):
        return RevGradFun.apply(input_, self._alpha)

class BertClassifier(torch.nn.Module):

    def __init__(self, n_classes, bert_model_name, adversarial=False):
        super(BertClassifier, self).__init__()
        bert_config = AutoConfig.from_pretrained(bert_model_name)
        self.bert_model = AutoModel.from_pretrained(bert_model_name, add_pooling_layer=True)
        self.dropout = torch.nn.Dropout(bert_config.hidden_dropout_prob)
        self.classifier = torch.nn.Linear(bert_config.hidden_size, n_classes)
        self.rev_grad = RevGrad()
        self.adversarial = adversarial

    def forward(self, input_ids, token_ids, attention_mask):
        outputs = self.bert_model(input_ids, attention_mask=attention_mask,
                                 token_type_ids=token_ids,
                                 position_ids=None, head_mask=None)
        pooled_output = outputs[1]
        pooled_output = self.dropout(pooled_output)
        if self.adversarial:
            pooled_output = self.rev_grad(pooled_output)

        logits = self.classifier(pooled_output)
        probas = F.softmax(logits, dim=1)
        return logits, probas
