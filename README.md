# covid-moral-foundations

This repository contains the code and data for our NAACL 2022 paper **A Holistic Framework for Analyzing the COVID-19 Vaccine Debate**. If you build on these ideas, or if you use any portion of our code or data, please cite us!

```
@inproceedings{pacheco-etal-2022-holistic,
    title = "A Holistic Framework for Analyzing the {COVID}-19 Vaccine Debate",
    author = "Pacheco, Maria Leonor  and
      Islam, Tunazzina  and
      Mahajan, Monal  and
      Shor, Andrey  and
      Yin, Ming  and
      Ungar, Lyle  and
      Goldwasser, Dan",
    booktitle = "Proceedings of the 2022 Conference of the North American Chapter of the Association for Computational Linguistics: Human Language Technologies",
    month = jul,
    year = "2022",
    address = "Seattle, United States",
    publisher = "Association for Computational Linguistics",
    url = "https://aclanthology.org/2022.naacl-main.427",
    pages = "5821--5839",
}
```


## Setting up your environment

### Install general dependencies

* Install virtualenv if you do not have it (you just need to do this
  once)

`pip3 --user install virtualenv`

* Create virtual environment (you just need to do this once)

`virtualenv envcovid`

* Activate virtual environment (every time you wish to run this setup)

`source envcovid/bin/activate`

* Install requirements (you just need to do this once)

`pip3 install -r requirements.txt`

### Install and setup DRaiL

* Install gurobi. You can skip this step if running in our lab machines (which you should do!),
  as gurobi is already installed.

* Request an academic license for gurobi and activate it

Go [here](https://www.gurobi.com/downloads/end-user-license-agreement-academic/) and follow instructions to request and activate your academic license. In the lab machines, you can find gurobi under `/opt/gurobi/`, and therefore you can do `/opt/gurobi/bin/grbgetkey [your_key]` to download the license to your home directory. 

Note that you will need a different license file per machine, so make sure to request more licenses and activate each of them in its respective machine. If you use a shared home directory accross machines (standard in our lab), make sure to change the name of the license file in your home directory before activating a new one so that it is not overwritten. 

* Update your environmental vars (if you are running this a lot, add
  this to your `.bashrc`)

```
export GUROBI_HOME=/opt/gurobi
export PATH=${PATH}:${GUROBI_HOME}/bin
export PYTHONPATH=${GUROBI_HOME}/lib/python3.8_utf32:${PYTHONPATH}
export GRB_LICENSE_FILE=$HOME/gurobi.lic
```

**Tip**: If you regularly use more than one lab machine with a shared home directory, you can use this trick in your `.bashrc` (after naming your license files accordingly):

```
case $HOSTNAME in 
  (hostname1.school.edu)
    export GRB_LICENSE_FILE=$HOME/gurobi.lic.hostname1;;
  (hostname2.school.edu)
    export GRB_LICENSE_FILE=$HOME/gurobi.lic.hostname2;;
esac
```

* Clone the DRaiL repository and point your PYTHONPATH variable to the DRaiL directory. 

```
git clone https://gitlab.com/purdueNlp/DRaiL
export PYTHONPATH=${PYTHONPATH}:/path/to/cloned/DRaiL
```

**Whenever you are done running these programs, you can deactivate the
virtualenv by typing:** `deactivate`.

## Downloading data

Download all the needed data from [here](https://www.dropbox.com/sh/vcc9jmagq28m6kj/AACmWkXmu2LjYQXgAwEijqNMa?dl=0). 

**Annotated COVID-19 dataset**: To obtain the text of the tweets needed for running the distance supervision baselines below, you will need to use the Twitter API to obtain the text given the tweet ID, and expand each tweet in `covid_annotated_tweets.json` with a `text` field. 

**Third-party datasets**: The external datasets derived from [congresstweets](https://github.com/alexlitel/congresstweets) and other open sources, are included directly given that the text is public (i.e. `kristen_ds/`, `liberty_oppression_data.csv`, `mpqaentsent.csv`). However, to obtain the tweets in the Moral Foundation Twitter Corpus `MFTC_V4.json` referenced below, you will need to hydrate the file `MFTC_V4_notext.json` using the Twitter API  and include a `text` field for each tweet. 

**Unlabed and weakly-labeled COVID-19 datasets**: These datasets can be found in subdirs `2021_vaccine_tweets` (tweets in English about COVID), `2021_vaccine_tweets_US` (tweets in English about COVID written in the US), `2021_provax_tweets` and `2021_antivax_tweets` (tweets in English collected hashtags listed in `data_collection/stance_hashtags.json`). In all cases, each tweet will need to be hydrated to include a `text` field using the Twitter API.

**Preprocessed files**: To run DRaiL scripts easily, the preprocessed files that map our internal tweet and entity IDs to bert tokens have been included in the data directory above (`tweet2bert.json` and `entity2bert.json`). To generate the `2021_US_tweets_text.npy` and `2021_US_tweets_bert.npy` referenced below, run (This has to be done after hydrating the unlabeled dataset and including a `text` field):

```
python3 data_extraction/sentence_embedding.py --tweets_path data/2021_vaccine_tweets_US --embed_out data/2021_US_tweets_bert.npy --tweet_out data/2021_US_tweets_text.npy
```

## Running distant supervision baselines

Input parameters make reference to the data downloaded in the previous
step. For PurdueNLP members, these are also available in our shared directory (in this case,
replace `data/` with our shared directory path -- or any where you copied the data in our shared directory). All of these targets
assume that you have a GPU available. If you wish to specify a GPU
index (e.g. GPU 1), prepend `CUDA_VISIBLE_DEVICES=1` to your command.
(e.g. ``CUDA_VISIBLE_DEVICES=1 python3 ...``)

### Entity role classifier

Feel free to change the output model path to wherever you want to
save the trained model to.

```
python3 run_role_classifier.py \
  --political_path data/kristen_ds \
  --covid_path data/covid_annotated_tweets.json \
  --no_sentiment \
  --out_model /tmp/role_distant.pt \
```
### Entity sentiment classifier

```
python3 run_entity_sentiment.classifer.py \
  --political_path data/kristen_ds \
  --mpqa_path data/mpqaentsent.csv \
  --covid_path data/covid_annotated_tweets.json \
  --out_model /tmp/sentiment_distant.pt \
```

### Moral foundation classifier

```
python3 run_mf_classifier.py \
  --political_path data/kristen_ds \
  --mftc_path data/MFTC_V4.json \
  --liberty_path data/liberty_opression_data.csv \
  --covid_path data/covid_annotated_tweets.json \
  --out_model /tmp/mf_distant.pt\
  --do_train \
  --do_eval
```

### Morality classifier

```
python3 run_mf_classifier.py \
  --political_path data/kristen_ds \
  --mftc_path data/MFTC_V4.json \
  --liberty_path data/liberty_opression_data.csv \
  --covid_path data/covid_annotated_tweets.json \
  --out_model /tmp/morality_distant.pt \
  --ismoral \
  --do_train \
  --do_eval
```

### Stance classifier

This script will also generate a `correlations.png` file showing
correlations between stance and moral foundations. If you just want to
obtain this file, and not run the train/test, remove the `--do_train`
and `--do_eval` parameters.

```
python3 run_stance_classifier.py \
  --pos_stance_path data/2021_provax_tweets \
  --neg_stance_path data/2021_antivax_tweets \
  --hashtags data_collection/stance_hashtags.json \
  --covid_path data/covid_annotated_tweets.json \
  --out_model /tmp/mf_distant.pt \
  --do_train \
  --do_eval
```

## Running base DRaiL models (supervised)

This target will run the base supervised classifiers, without any dependencies or
constraints. Feel free to change the `savedir` directory to wherever you
want to save your trained models. Once you have trained it, you can
re-run the evaluation by removing the `--do_train` parameter and point to
the same `--savedir`.

```
python3 run_drail.py \
  -r drail_programs/rules/supervised_base.dr \
  -c drail_programs/config/config_base.json \
  --savedir /tmp/supervised_base/ \
  --folds data/covid_annotated_folds.json \
  --tweet2bert data/tweet2bert.json \
  --entity2bert data/entity2bert.json \
  --do_train \
  --do_eval
```

## Running full DRaiL model (supervised)

This target will run the full DRaiL model, with all dependencies and
constraints. Once you have trained it, you can re-run the evaluation by
removing the `--do_train` parameter and point to the same `--savedir`.

```
python3 run_drail.py \
  -r drail_programs/rules/supervised_all.dr \
  -c drail_programs/config/config_all.json \
  --savedir /tmp/supervised_all/ \
  --folds data/covid_annotated_folds.json \
  --tweet2bert data/tweet2bert.json \
  --entity2bert data/entity2bert.json \
  --do_train \
  --do_eval
```

## Running distant supervision model with DRaiL

**TO-DO**

## Running additional viz and analyses

#### Running LDA topic model and generating heatmap matrices


```
python3 run_topic_models.py \
  --unlabeled_data data/2021_US_tweets_text.npy \
  --tweets data/covid_annotated_tweets.json
```

This target will generate the following files:

```
has_theme_0.3_mf_correlations.png
has_theme_0.3_stance_correlations.png
has_theme_mf_correlationss.png
has_theme_stance_correlations.png
has_theme_original_mf_correlations.png
has_theme_original_stance_correlations.png
lda_mf_correlations.png
lda_stance_correlations.png
```

#### See top entities and roles per reason

**TO-DO**
