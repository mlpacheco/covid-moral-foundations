import numpy as np
import random
import argparse
from gensim.corpora.dictionary import Dictionary
from gensim.utils import tokenize
from gensim.models.ldamodel import LdaModel
from gensim.parsing.preprocessing import remove_stopwords
import json
from collections import Counter
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import seaborn as sn
import matplotlib.pyplot as plt


def get_stance_correlation(topic_labels, stance_labels):
    topic_label_set = set(topic_labels)

    topic_variables = {}
    for lbl in topic_label_set:
        topic_variables[lbl] = []
    stance_variables = {'pro-vax': [], 'anti-vax': [], 'neutral': [], 'stance-unknown': []}
    stance_idx_map = ['pro-vax', 'anti-vax', 'neutral', 'stance-unknown']

    for topic_lbl, stance_lbl in zip(topic_labels, stance_labels):
        for var in topic_variables:
            if topic_lbl == var:
                topic_variables[var].append(1)
            else:
                topic_variables[var].append(0)
        for var in stance_variables:
            if stance_idx_map[stance_lbl] == var:
                stance_variables[var].append(1)
            else:
                stance_variables[var].append(0)
    df1 = pd.DataFrame(topic_variables, columns=topic_variables.keys())
    df2 = pd.DataFrame(stance_variables, columns=['pro-vax', 'anti-vax'])
    #corrMatrix = corr(df2, df1)
    corrMatrix = pd.concat([df2, df1], axis=1, keys=['df2', 'df1']).corr().loc['df1', 'df2']
    return corrMatrix

def get_mf_correlation(topic_labels, mf_labels):
    topic_label_set = set(topic_labels)

    topic_variables = {}
    for lbl in topic_label_set:
        topic_variables[lbl] = []

    mf_label_set = set(mf_labels)
    mf_variables = {}
    for lbl in mf_label_set:
        mf_variables[lbl] = []

    for topic_lbl, mf_lbl in zip(topic_labels, mf_labels):
        for var in topic_variables:
            if topic_lbl == var:
                topic_variables[var].append(1)
            else:
                topic_variables[var].append(0)
        for var in mf_variables:
            if mf_lbl == var:
                mf_variables[var].append(1)
            else:
                mf_variables[var].append(0)

    df1 = pd.DataFrame(topic_variables, columns=topic_variables.keys())
    df2 = pd.DataFrame(mf_variables, columns=mf_variables.keys())
    #corrMatrix = corr(df2, df1)
    corrMatrix = pd.concat([df2, df1], axis=1, keys=['df2', 'df1']).corr().loc['df1', 'df2']
    return corrMatrix


def extract_topics(args):
    documents = np.load(args.unlabeled_data)
    documents = [list(tokenize(remove_stopwords(d), lowercase=True)) for d in documents]
    dictionary = Dictionary(documents)
    corpus = [dictionary.doc2bow(text) for text in documents]

    lda = LdaModel(corpus, num_topics=22, id2word=dictionary)
    return dictionary, lda

def normalize_mf(mf):
    if mf == 'fairness/cheating':
        return 'fair/cheat'
    elif mf == 'liberty/oppression':
        return 'lib/opp'
    elif mf == 'authority/subversion':
        return 'auth/subv'
    elif mf == 'sanctity/degradation':
        return 'pure/degrade'
    elif mf == 'purity/degradation':
        return 'pure/degrade'
    elif mf == 'loyalty/betrayal':
        return 'loyal/betray'
    else:
        return mf

def classify_tweets(args, dictionary, lda):
    tweets = json.load(open(args.tweets))
    documents = []; stances = []; mfs = []
    for twid in tweets:
        if tweets[twid]['stance'] is None or tweets[twid]['stance'] > 1 or tweets[twid]['mf'] is None:
            continue
        documents.append(tweets[twid]['text'])
        stances.append(tweets[twid]['stance'])
        mf = normalize_mf(tweets[twid]['mf'])
        mfs.append(mf)

    documents = [list(tokenize(remove_stopwords(d), lowercase=True)) for d in documents]
    dictionary = Dictionary(documents)
    corpus = [dictionary.doc2bow(text) for text in documents]

    topics = []
    for unseen_doc in corpus:
        vector = lda[unseen_doc]
        max_score = -1; max_topic = None
        for (topic, score) in vector:
            if score > max_score:
                max_topic = topic
                max_score = score
        topics.append(max_topic)
    topics = ["topic-{}".format(x) for x in topics]

    corrMatrix = get_stance_correlation(topics, stances)
    sn.heatmap(corrMatrix, annot=True, cmap='coolwarm', center=0, vmin=-0.25, vmax=0.25, square=False)
    plt.subplots_adjust(left=0.35)
    plt.plot()
    plt.savefig('lda_stance_correlations.png')
    plt.close()
    plt.cla()
    plt.clf()

    corrMatrix = get_mf_correlation(topics, mfs)
    ax = sn.heatmap(corrMatrix, annot=False, cmap='coolwarm', center=0, vmax=0.55, vmin=-0.20, square=False)
    ax.xaxis.tick_top()
    ax.tick_params(axis='x', labelrotation=90)
    plt.tight_layout()
    plt.plot()
    plt.savefig('lda_mf_correlations.png')
    plt.close()
    plt.cla()
    plt.clf()


def plot_reasons(args, theme_file):
    id2theme = {}
    with open('drail_programs/data/{}.txt'.format(theme_file)) as fp:
        for line in fp:
            _id, _theme = line.strip().split('\t')
            id2theme[_id] = _theme
    id2stance = {}
    with open('drail_programs/data/has_stance.txt') as fp:
        for line in fp:
            _id, _stance = line.strip().split('\t')
            id2stance[_id] = int(_stance)
    id2mf = {}
    with open('drail_programs/data/has_mf.txt') as fp:
        for line in fp:
            _id, _mf = line.strip().split('\t')
            id2mf[_id] = normalize_mf(_mf)

    topics = []; stances = []; mfs = []
    for _id in id2theme:
        if _id in id2stance and _id in id2mf:
            topics.append(id2theme[_id])
            stances.append(id2stance[_id])
            mfs.append(id2mf[_id])

    corrMatrix = get_stance_correlation(topics, stances)
    sn.heatmap(corrMatrix, annot=True, cmap='coolwarm', center=0, vmin=-0.25, vmax=0.25, square=False)
    plt.subplots_adjust(left=0.35)
    plt.plot()
    plt.savefig('{}_stance_correlations.png'.format(theme_file))
    plt.close()
    plt.cla()
    plt.clf()

    corrMatrix = get_mf_correlation(topics, mfs)
    ax = sn.heatmap(corrMatrix, annot=False, cmap='coolwarm', center=0, vmax=0.55, vmin=-0.20, square=False)
    #plt.subplots_adjust(left=0.25)
    ax.xaxis.tick_top()
    ax.tick_params(axis='x', labelrotation=90)
    #plt.subplots_adjust(left=0.35, )
    plt.tight_layout()
    plt.plot()
    plt.savefig('{}_mf_correlations.png'.format(theme_file))
    plt.close()
    plt.cla()
    plt.clf()

def main(args):
    dictionary, lda = extract_topics(args)
    classify_tweets(args, dictionary, lda)
    plot_reasons(args, 'has_theme')
    plot_reasons(args, 'has_theme_0.3')
    plot_reasons(args, 'has_theme_original')

if __name__ == "__main__":
    np.random.seed(1534)
    random.seed(1534)

    parser = argparse.ArgumentParser()
    parser.add_argument('--unlabeled_data', type=str, required=True)
    parser.add_argument('--tweets', type=str, required=True)
    args = parser.parse_args()
    main(args)
