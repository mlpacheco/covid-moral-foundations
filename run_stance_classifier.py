import argparse
import torch
import json
from torch.utils import data
import os
import numpy as np
from tqdm.auto import tqdm
from collections import Counter
from transformers import AutoTokenizer
from transformers import get_scheduler
from transformers import AdamW
from sklearn.metrics import f1_score, confusion_matrix, classification_report
from sklearn.utils.class_weight import compute_class_weight
from models.bert_classifier import BertClassifier
from sklearn.preprocessing import LabelEncoder
import pickle
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import seaborn as sn
import matplotlib.pyplot as plt
import random

class TextDataset(data.Dataset):

    def __init__(self, input_ids, token_ids, attention_mask, labels):
        self.input_ids = input_ids
        self.token_ids = token_ids
        self.attention_mask = attention_mask
        self.labels = labels

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, index):
        input_ids_i = self.input_ids[index]
        token_ids_i = self.token_ids[index]
        attention_mask_i = self.attention_mask[index]
        label = self.labels[index]

        return input_ids_i, token_ids_i, attention_mask_i, label

def prepare_stance_users_ds(args):
    tokenizer = AutoTokenizer.from_pretrained('bert-base-uncased')
    batch_sentences = [];
    stance_labels = []

    positive = open("positive_tweets.txt", "w")
    negative = open("negative_tweets.txt", "w")
    positive_index = 0; negative_index = 0

    positive_users = set(); negative_users = set()

    for path, label in zip([args.stance_neg_path, args.stance_pos_path], [1, 0]):
        dataset = json.load(open(path))
        for user in dataset:
            for tw in dataset[user]['tweets']:
                text = tw['text'].strip()
                if label == 1:
                    negative_users.add(user)
                    negative.write(str(negative_index) + "|")
                    negative_index += 1
                    negative.write(text)
                    negative.write("\n")
                else:
                    positive_users.add(user)
                    positive.write(str(positive_index) + "|")
                    positive_index += 1
                    positive.write(text)
                    positive.write("\n")

                batch_sentences.append(text)
                stance_labels.append(label)
    print("pos users", len(positive_users))
    print("neg users", len(negative_users))
    print("overlap", len(positive_users & negative_users))
    #exit()
    positive.close()
    negative.close()
    encoded_inputs = tokenizer(batch_sentences, padding='max_length', truncation=True, return_tensors='pt', max_length=80)

    input_ids = encoded_inputs['input_ids'].cuda()
    token_ids = encoded_inputs['token_type_ids'].cuda()
    attention_mask = encoded_inputs['attention_mask'].cuda()
    stance_labels = torch.LongTensor(stance_labels).cuda()

    return input_ids, token_ids, attention_mask,\
            stance_labels

def clean_hashtags(hashtags, hashtag_group, text):
    new_text = text
    for ht in hashtags[hashtag_group]:
        new_text = new_text.replace('#' + ht, '')
    #if text != new_text:
    #    print(new_text)
    #    print('-------')
    return text

def prepare_stance_hashtag_ds(args):
    tokenizer = AutoTokenizer.from_pretrained('bert-base-uncased')
    batch_pos = []; batch_neg = []
    hashtags = json.load(open(args.hashtags))
    for filename in os.listdir(args.neg_stance_path):
        filepath = os.path.join(args.neg_stance_path, filename)
        neg_tweets = json.load(open(filepath))
        pbar = tqdm(total=len(neg_tweets['tweets']), desc='Processing negative tweets - {}'.format(filename))
        for tw in neg_tweets['tweets']:
            text = tw['text'].strip()
            text = clean_hashtags(hashtags, 'antivax', text)
            batch_neg.append(text)
            pbar.update(1)
        pbar.close()
    for filename in os.listdir(args.pos_stance_path):
        filepath = os.path.join(args.pos_stance_path, filename)
        pos_tweets = json.load(open(filepath))
        pbar = tqdm(total=len(pos_tweets['tweets']), desc='Processing positive tweets - {}'.format(filename))
        for tw in pos_tweets['tweets']:
            text = tw['text'].strip()
            text = clean_hashtags(hashtags, 'provax', text)
            batch_pos.append(text)
            pbar.update(1)
        pbar.close()
    # Do we want to reduce number of positives?
    random.shuffle(batch_pos)
    batch_pos = batch_pos[:500000]

    batch_sentences = batch_neg + batch_pos
    stance_labels = [1] * len(batch_neg) + [0] * len(batch_pos)

    print("Encoding BERT inputs")

    encoded_inputs = tokenizer(batch_sentences, padding='max_length', truncation=True, return_tensors='pt', max_length=80)

    input_ids = encoded_inputs['input_ids'].cuda()
    token_ids = encoded_inputs['token_type_ids'].cuda()
    attention_mask = encoded_inputs['attention_mask'].cuda()
    stance_labels = torch.LongTensor(stance_labels).cuda()
    print("Done.")
    return input_ids, token_ids, attention_mask,\
            stance_labels


def prepare_covid_dataset(covid_path, args):
    tokenizer = AutoTokenizer.from_pretrained('bert-base-uncased')

    dataset = json.load(open(covid_path))

    batch_sentences = [];
    moral_foundation_labels = []
    moral_nonmoral_labels = []
    stance_labels = []

    for key in dataset:
        if args.implied_only and dataset[key]['stance_comments'] == "":
            continue
        if args.easy_only and dataset[key]['stance_comments'] != '':
            continue

        if dataset[key]['mf'] is None:
            text = dataset[key]['text']
            batch_sentences.append(text)
            moral_nonmoral_labels.append('moral')
            moral_foundation_labels.append('no-agreement')
        elif dataset[key]['mf'] == "none":
            text = dataset[key]['text']
            batch_sentences.append(text)
            moral_nonmoral_labels.append('non-moral')
            moral_foundation_labels.append('non-moral')
        elif dataset[key]['mf'] != "none":
            text = dataset[key]['text']
            batch_sentences.append(text)
            moral_foundation_labels.append(dataset[key]['mf'].strip().replace("sanctity", "purity"))
            moral_nonmoral_labels.append('moral')

        if dataset[key]['stance'] is None:
            # Use 3 to signal unknown
            stance_labels.append(3)
        else:
            stance_labels.append(dataset[key]['stance'])

    encoded_inputs = tokenizer(batch_sentences, padding=True, truncation=True, return_tensors='pt')

    input_ids = encoded_inputs['input_ids'].cuda()
    token_ids = encoded_inputs['token_type_ids'].cuda()
    attention_mask = encoded_inputs['attention_mask'].cuda()

    return input_ids, token_ids, attention_mask,\
           moral_nonmoral_labels, moral_foundation_labels, stance_labels


def train(dataset, dev_dataset, test_dataset, model, loss_fn, model_path):
    train_dataloader = data.DataLoader(dataset, batch_size=32, shuffle=True)
    optimizer = torch.optim.AdamW(model.parameters(), lr=2e-5)
    num_epochs = 3

    PAT = 1
    best_f1 = 0; patience = PAT
    while patience > 0:
        train_loss = 0; train_batches = 0
        y_pred_all = []; y_gold_all = []
        pbar = tqdm(range(len(train_dataloader)))

        model.train()
        for input_ids, token_ids, attention_mask, labels in train_dataloader:
            model.zero_grad()
            logits, probas = model(input_ids, token_ids, attention_mask)
            loss = loss_fn(logits, labels)
            train_loss += loss.item()
            train_batches += 1

            _, y_pred = torch.max(probas, 1)
            y_pred_all += list(y_pred.cpu())
            y_gold_all += list(labels.cpu())

            loss.backward()
            optimizer.step()

            pbar.update(1)
        pbar.close()
        train_loss = train_loss / train_batches
        wf1 = f1_score(y_gold_all, y_pred_all, average='weighted')
        mf1 = f1_score(y_gold_all, y_pred_all, average='macro')

        loss_dev, wf1_dev, mf1_dev = predict(dev_dataset, model, loss_fn, fold='Dev', batch_size=32)
        if wf1_dev > best_f1:
            best_f1 = wf1_dev
            patience = PAT
            torch.save(model.state_dict(), model_path)
        else:
            patience -= 1
        _, wf1_test, mf1_test = predict(test_dataset, model, loss_fn=None, fold='Test', batch_size=1)

        print("Train loss: {0}, weighted F1: {1}, macro F1: {2}".format(train_loss, wf1, mf1))

def predict(dataset, model, loss_fn, fold, print_report=False, batch_size=32):
    test_dataloader = data.DataLoader(dataset, batch_size=batch_size, shuffle=True)

    model.eval()
    pbar = tqdm(range(len(test_dataloader)))
    test_loss = 0; test_batches = 0
    y_pred_all = []; y_gold_all = []

    for input_ids, token_ids, attention_mask, labels in test_dataloader:
        logits, probas = model(input_ids, token_ids, attention_mask)

        if loss_fn is not None:
            loss = loss_fn(logits, labels)
            test_loss += loss.item()

        test_batches += 1
        _, y_pred = torch.max(probas, 1)
        y_pred_all += list(y_pred.cpu())
        y_gold_all += list(labels.cpu())

        pbar.update(1)

    pbar.close()
    test_loss = test_loss / test_batches
    wf1 = f1_score(y_gold_all, y_pred_all, average='weighted', labels=[0, 1])
    mf1 = f1_score(y_gold_all, y_pred_all, average='macro', labels=[0, 1])

    print("{0} loss: {1}, weighted F1: {2}, macro F1: {3}".format(fold, test_loss, wf1, mf1))
    if print_report:
        print(confusion_matrix(y_gold_all, y_pred_all, labels=[0, 1]))
        print(classification_report(y_gold_all, y_pred_all, labels=[0, 1], target_names=['pro', 'anti'], digits=4))
    return test_loss, wf1, mf1

def predict_nolabels(dataset, model):
    test_dataloader = data.DataLoader(dataset, batch_size=32, shuffle=True)

    model.eval()
    pbar = tqdm(range(len(test_dataloader)))
    y_pred_stance = []; y_gold_mf = []
    for input_ids, token_ids, attention_mask, labels in test_dataloader:
        logits, probas = model(input_ids, token_ids, attention_mask)
        _, y_pred = torch.max(probas, 1)
        y_pred_stance += list(y_pred.cpu())
        y_gold_mf += list(labels.cpu())

        pbar.update(1)

    pbar.close()
    return y_pred_stance, y_gold_mf

def compute_weights(Y, output_dim):
    counts = np.bincount(Y)
    num_missing = output_dim - len(counts)
    counts = np.append(counts, np.asarray([0]*num_missing))

    # smoothing
    counts[counts == 0] = 1
    #compute weights
    weights = np.asarray(np.max(counts)*1.0/counts, dtype = "float32")
    return weights

def corr(df1, df2):
    n = len(df1)
    v1, v2 = df1.values, df2.values
    sums = np.multiply.outer(v2.sum(0), v1.sum(0))
    stds = np.multiply.outer(v2.std(0), v1.std(0))
    return pd.DataFrame((v2.T.dot(v1) - sums / n) / stds / n,
            df2.columns, df1.columns)


def get_correlation(mf_labels, stance_labels):
    mf_label_set = set(mf_labels) - set(['no-agreement'])
    mf_variables = {}
    for lbl in mf_label_set:
        mf_variables[lbl] = []
    stance_variables = {'pro-vax': [], 'anti-vax': [], 'neutral': [], 'stance-unknown': []}
    stance_idx_map = ['pro-vax', 'anti-vax', 'neutral', 'stance-unknown']

    for mf_lbl, stance_lbl in zip(mf_labels, stance_labels):
        if mf_lbl == 'no-agreement' or stance_lbl > 1:
            continue
        for var in mf_variables:
            if mf_lbl == var:
                mf_variables[var].append(1)
            else:
                mf_variables[var].append(0)
        for var in stance_variables:
            if stance_idx_map[stance_lbl] == var:
                stance_variables[var].append(1)
            else:
                stance_variables[var].append(0)
    df1 = pd.DataFrame(mf_variables, columns=mf_variables.keys())
    df2 = pd.DataFrame(stance_variables, columns=['pro-vax', 'anti-vax'])
    #corrMatrix = corr(df2, df1)
    corrMatrix = pd.concat([df2, df1], axis=1, keys=['df2', 'df1']).corr().loc['df1', 'df2']
    return corrMatrix

def normalize_mf(mf):
    if mf == 'fairness/cheating':
        return 'fair/cheat'
    elif mf == 'liberty/oppression':
        return 'lib/opp'
    elif mf == 'authority/subversion':
        return 'auth/subv'
    elif mf == 'sanctity/degradation':
        return 'pure/degrade'
    elif mf == 'purity/degradation':
        return 'pure/degrade'
    elif mf == 'loyalty/betrayal':
        return 'loyal/betray'
    else:
        return mf

def main(args):
    if args.do_train:
        print("Loading stance data")
        '''
        input_ids, token_ids, attention_mask,\
            stance_labels = prepare_stance_users_ds(args)
        '''
        input_ids, token_ids, attention_mask,\
            stance_labels = prepare_stance_hashtag_ds(args)

        train_dataset = TextDataset(input_ids, token_ids, attention_mask, stance_labels)
        n_train = int(0.8 * len(train_dataset))
        n_dev = len(train_dataset) - n_train
        train_dataset, dev_dataset = torch.utils.data.random_split(train_dataset, [n_train, n_dev])
        print(len(train_dataset), len(dev_dataset))
        #exit()

    input_ids, token_ids, attention_mask,\
        moral_test_labels, mf_test_labels, stance_test_labels = prepare_covid_dataset(args.covid_path, args)

    corr_mf_labels = [normalize_mf(mf) for mf in mf_test_labels]
    corrMatrix = get_correlation(corr_mf_labels, stance_test_labels)
    sn.heatmap(corrMatrix, annot=True, cmap='coolwarm', center=0, vmin=-0.25, vmax=0.25)
    plt.subplots_adjust(left=0.28)
    plt.plot()
    plt.savefig('correlations.png')

    test_labels = torch.LongTensor(stance_test_labels).cuda()

    test_dataset = TextDataset(input_ids, token_ids, attention_mask, test_labels)
    test_dataloader = data.DataLoader(test_dataset, batch_size=32, shuffle=True)

    model = BertClassifier(2, 'bert-base-uncased')
    model.cuda()

    if args.do_train:
        # get class weights based on training set
        cws = compute_weights(stance_labels.cpu().numpy(), 2)
        cws = torch.FloatTensor(cws).cuda()

        loss_fn = torch.nn.CrossEntropyLoss(weight=cws)
        train(train_dataset, dev_dataset, test_dataset, model, loss_fn, args.out_model)

    if args.do_eval:
        model.load_state_dict(torch.load(args.out_model))
        #predict(dev_dataset, model, loss_fn, fold='DEV', print_report=True)
        predict(test_dataset, model, loss_fn=None, fold='TEST', print_report=True)


if __name__ == "__main__":
    # reproducibility
    torch.manual_seed(42)
    np.random.seed(42)

    parser = argparse.ArgumentParser()
    parser.add_argument('--pos_stance_path', type=str, required=True)
    parser.add_argument('--neg_stance_path', type=str, required=True)
    parser.add_argument('--hashtags', type=str, required=True)
    parser.add_argument('--out_model', type=str, required=True)
    parser.add_argument('--covid_path', type=str, required=True)
    parser.add_argument('--do_train', default=False, action='store_true')
    parser.add_argument('--do_eval', default=False, action='store_true')
    parser.add_argument('--implied_only', default=False, action='store_true')
    parser.add_argument('--easy_only', default=False, action='store_true')
    args = parser.parse_args()
    main(args)

