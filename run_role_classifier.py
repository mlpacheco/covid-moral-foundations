import argparse
import torch
import json
from torch.utils import data
import os
import numpy as np
from tqdm.auto import tqdm
from collections import Counter
from transformers import AutoTokenizer
from transformers import get_scheduler
from transformers import AdamW
from sklearn.metrics import f1_score, confusion_matrix, classification_report
from sklearn.utils.class_weight import compute_class_weight
from models.bert_classifier import BertClassifier
from sklearn.preprocessing import LabelEncoder
import pickle

class TextDataset(data.Dataset):

    def __init__(self, input_ids, token_ids, attention_mask, labels, labels_2=None):
        self.input_ids = input_ids
        self.token_ids = token_ids
        self.attention_mask = attention_mask
        self.labels = labels
        self.labels_2 = labels

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, index):
        input_ids_i = self.input_ids[index]
        token_ids_i = self.token_ids[index]
        attention_mask_i = self.attention_mask[index]
        label = self.labels[index]

        if self.labels_2 is not None:
            label_2 = self.labels_2[index]
            return input_ids_i, token_ids_i, attention_mask_i, label, label_2

        return input_ids_i, token_ids_i, attention_mask_i, label

def prepare_political_dataset(political_path, args):
    tokenizer = AutoTokenizer.from_pretrained('bert-base-uncased')

    entity2role = {}; entity2tweet = {}
    with open(os.path.join(political_path, 'has_role.txt')) as fp:
        for line in fp:
            (tweet, entity, role) = line.strip().split('\t')
            if role == 'care-q1':
                new_role = 'target'
            elif role == 'care-q2':
                new_role = 'neg_actor'
            elif role == 'care-q3':
                new_role = 'pos_actor'
            elif role == 'fairness-q2':
                new_role = 'target'
            elif role == 'fairness-q3':
                new_role = 'pos_actor'
            elif role == 'fairness-q4':
                new_role = 'neg_actor'
            elif role == 'loyalty-q3':
                new_role = 'target'
            elif role == 'loyalty-q4':
                new_role = 'pos_actor'
            elif role == 'loyalty-q5':
                new_role = 'neg_actor'
            elif role == 'authority-q2':
                new_role = 'pos_actor'
            elif role == 'authority-q3' or role == 'authority-q4':
                new_role = 'target'
            elif role == 'authority-q5':
                new_role = 'neg_actor'
            elif role == 'sanctity-q1':
                new_role = 'target'
            elif role == 'sanctity-q2':
                new_role = 'pos_actor'
            elif role == 'sanctity-q3':
                new_role = 'neg_actor'
            else:
                print(role)
                exit()
            entity2role[entity] = new_role
            entity2tweet[entity] = tweet

    tw2topic = {}
    with open(os.path.join(political_path, 'has_topic.txt')) as fp:
        for line in fp:
            (tweet, topic) = line.strip().split('\t')
            tw2topic[tweet] = topic

    metadata = json.load(open(os.path.join(political_path, "metadata.json")))
    key2text, key2ideo, key2mf_prominent_polarity, key2mf_prominent, key2mf_all, key2topic, key2time = metadata

    role_labels = []
    topic_labels = []

    mf_set = set()

    encoded_prev = {'input_ids': [], 'token_type_ids': [], 'attention_mask': []}

    data_f = os.path.join(political_path, 'drail_data.pickle')
    with open(data_f, 'rb') as fp:
        [tweet_id2tokens, entity_id2tokens,\
         ideology_id2one_hot,\
         word_dict, vocab_size, word_emb_size, word2idx]  = pickle.load(fp, encoding="latin1")
        idx2word = {v: k for k, v in word2idx.items()}

    data_bert_f = os.path.join(political_path, 'drail_data_bert.pickle')
    with open(data_bert_f, 'rb') as fp:
        [tweet_id2bert, entity_id2bert, ideology_id2one_hot] = pickle.load(fp, encoding="latin1")

    for entity in entity2role:
        if entity2role[entity] == 'target' and args.no_role:
            continue
        tw = entity2tweet[entity]
        encoded_prev['input_ids'].append(tweet_id2bert[tw]['input_ids'] + entity_id2bert[entity]['input_ids'][1:])
        encoded_prev['token_type_ids'].append([0] * len(tweet_id2bert[tw]['input_ids']) + [1] * len(entity_id2bert[entity]['input_ids'][1:]))
        encoded_prev['attention_mask'].append(tweet_id2bert[tw]['attention_mask'] + entity_id2bert[entity]['attention_mask'][1:])
        role_labels.append(entity2role[entity])
        topic_labels.append(tw2topic[tw])
    #print(np.array(encoded_prev['input_ids']).shape)
    #print(np.array(encoded_prev['token_type_ids']).shape)
    #print(np.array(encoded_prev['attention_mask']).shape)

    input_ids = encoded_prev['input_ids']
    token_ids = encoded_prev['token_type_ids']
    attention_mask = encoded_prev['attention_mask']

    return input_ids, token_ids, attention_mask,\
            role_labels, topic_labels

def prepare_covid_dataset(covid_path, args):
    tokenizer = AutoTokenizer.from_pretrained('bert-base-uncased')

    dataset = json.load(open(covid_path))

    batch_sentences = []; batch_entities = []
    role_labels = []

    mf_set = set()

    for key in dataset:
        text = dataset[key]['text']
        for actor in dataset[key]['pos_actors']:
            batch_sentences.append(text)
            batch_entities.append(actor)
            role_labels.append('pos_actor')
        for actor in dataset[key]['neg_actors']:
            batch_sentences.append(text)
            batch_entities.append(actor)
            role_labels.append('neg_actor')
        else:
            if args.no_role:
                 for target in dataset[key]['pos_targets']:
                     batch_sentences.append(text)
                     batch_entities.append(actor)
                     role_labels.append('pos_target')
                 for target in dataset[key]['neg_targets']:
                     batch_sentences.append(text)
                     batch_entities.append(actor)
                     role_labels.append('neg_target')
            else:
                for target in dataset[key]['pos_targets'] + dataset[key]['neg_targets']:
                    batch_sentences.append(text)
                    batch_entities.append(target)
                    role_labels.append('target')

    encoded_inputs = tokenizer(batch_sentences, batch_entities, padding=True, truncation=True, return_tensors='pt')

    input_ids = encoded_inputs['input_ids'].cuda()
    token_ids = encoded_inputs['token_type_ids'].cuda()
    attention_mask = encoded_inputs['attention_mask'].cuda()

    return input_ids, token_ids, attention_mask,\
            role_labels

def train(dataset, dev_dataset, model, loss_fn, le, model_adv, loss_fn_adv, out_model):
    train_dataloader = data.DataLoader(dataset, batch_size=32, shuffle=True)
    optimizer = torch.optim.AdamW(model.parameters(), lr=2e-5)
    num_epochs = 3

    PAT = 2
    best_f1 = 0; patience = PAT
    while patience > 0:
        train_loss = 0; train_batches = 0
        y_pred_all = []; y_gold_all = []
        pbar = tqdm(range(len(train_dataloader)))

        model.train()
        #model_adv.train()
        for input_ids, token_ids, attention_mask, labels, topic_labels in train_dataloader:
            model.zero_grad()
            logits, probas = model(input_ids, token_ids, attention_mask)
            loss = loss_fn(logits, labels)
            train_loss += loss.item()
            train_batches += 1

            if model_adv is not None:
                model_adv.zero_grad()
                logits, _ = model_adv(input_ids, token_ids, attention_mask)
                loss_adv = loss_fn_adv(logits, topic_labels)
                loss += loss_adv

            _, y_pred = torch.max(probas, 1)
            y_pred_all += list(y_pred.cpu())
            y_gold_all += list(labels.cpu())

            loss.backward()
            optimizer.step()

            pbar.update(1)
        pbar.close()
        train_loss = train_loss / train_batches
        wf1 = f1_score(y_gold_all, y_pred_all, average='weighted')
        mf1 = f1_score(y_gold_all, y_pred_all, average='macro')

        loss_dev, wf1_dev, mf1_dev = predict(dev_dataset, model, loss_fn, le)
        if wf1_dev > best_f1:
            best_f1 = wf1_dev
            patience = PAT
            torch.save(model.state_dict(), out_model)
        else:
            patience -= 1

        print("Train loss: {0}, weighted F1: {1}, macro F1: {2}".format(train_loss, wf1, mf1))
        #print("Dev Perfm: loss: {1}, weighted F1: {2}, macro F1: {3}".format(loss_dev, wf1_dev, mf1_dev))

def predict(dataset, model, loss_fn, le, print_report=False):
    test_dataloader = data.DataLoader(dataset, batch_size=32, shuffle=True)

    model.eval()
    pbar = tqdm(range(len(test_dataloader)))
    test_loss = 0; test_batches = 0
    y_pred_all = []; y_gold_all = []

    for input_ids, token_ids, attention_mask, labels, topic_labels in test_dataloader:
        logits, probas = model(input_ids, token_ids, attention_mask)
        loss = loss_fn(logits, labels)
        test_loss += loss.item()
        test_batches += 1

        _, y_pred = torch.max(probas, 1)
        y_pred_all += list(y_pred.cpu())
        y_gold_all += list(labels.cpu())

        pbar.update(1)

    pbar.close()
    test_loss = test_loss / test_batches
    wf1 = f1_score(y_gold_all, y_pred_all, average='weighted')
    mf1 = f1_score(y_gold_all, y_pred_all, average='macro')

    print("Test loss: {0}, weighted F1: {1}, macro F1: {2}".format(test_loss, wf1, mf1))
    if print_report:
        print(confusion_matrix(y_gold_all, y_pred_all))
        print(classification_report(y_gold_all, y_pred_all, target_names=le.classes_, digits=4))
    return test_loss, wf1, mf1

def compute_weights(Y, output_dim):
    counts = np.bincount(Y)
    num_missing = output_dim - len(counts)
    counts = np.append(counts, np.asarray([0]*num_missing))

    # smoothing
    counts[counts == 0] = 1
    #compute weights
    weights = np.asarray(np.max(counts)*1.0/counts, dtype = "float32")
    return weights


def main(args):
    input_ids = []; token_ids = []; attention_mask = []

    if args.political_path is not None:
        print("Loading political path")
        input_ids, token_ids, attention_mask,\
            role_train_labels, topic_train_labels = prepare_political_dataset(args.political_path, args)

    if args.no_sentiment:
        role_train_labels = [x.replace('pos_actor', 'actor').replace('neg_actor', 'actor') for x in role_train_labels]
        n_classes = 2
    if args.no_role:
        role_train_labels = [x.replace('pos_actor', 'pos').replace('neg_actor', 'neg').replace('pos_target', 'pos').replace('neg_target', 'neg') for x in role_train_labels]
        n_classes = 2

    input_ids = torch.LongTensor(input_ids).cuda()
    token_ids = torch.LongTensor(token_ids).cuda()
    attention_mask = torch.LongTensor(attention_mask).cuda()

    le = LabelEncoder()
    train_labels = le.fit_transform(role_train_labels)
    train_labels = torch.LongTensor(train_labels).cuda()

    le_topic = LabelEncoder()
    topic_labels = le_topic.fit_transform(topic_train_labels)
    topic_labels = torch.LongTensor(topic_labels).cuda()

    train_dataset = TextDataset(input_ids, token_ids, attention_mask, train_labels, topic_labels)
    n_train = int(0.8 * len(train_dataset))
    n_dev = len(train_dataset) - n_train
    train_dataset, dev_dataset = torch.utils.data.random_split(train_dataset, [n_train, n_dev])
    print(len(train_dataset), len(dev_dataset))
    #exit()

    input_ids, token_ids, attention_mask,\
        role_test_labels = prepare_covid_dataset(args.covid_path, args)
    if args.no_sentiment:
        role_test_labels = [x.replace('pos_actor', 'actor').replace('neg_actor', 'actor') for x in role_test_labels]
    if args.no_role:
        role_test_labels = [x.replace('pos_actor', 'pos').replace('neg_actor', 'neg').replace('pos_target', 'pos').replace('neg_target', 'neg') for x in role_test_labels]
    print("train", Counter(role_train_labels))
    print("test", Counter(role_test_labels))
    print("Train Topic Stats", Counter(topic_train_labels), len(topic_train_labels))
    test_labels = le.transform(role_test_labels)

    test_labels = torch.LongTensor(test_labels).cuda()

    test_dataset = TextDataset(input_ids, token_ids, attention_mask, test_labels)
    test_dataloader = data.DataLoader(test_dataset, batch_size=32, shuffle=True)

    # get class weights based on training set
    cws = compute_weights(train_labels.cpu().numpy(), n_classes)
    cws = torch.FloatTensor(cws).cuda()

    loss_fn = torch.nn.CrossEntropyLoss(weight=cws)
    model = BertClassifier(n_classes, 'bert-base-uncased')
    model.cuda()

    model_adv = None; loss_fn_adv = None
    if args.adversarial:
        cws_topic = compute_weights(topic_labels.cpu().numpy(), n_classes)
        cws_topic = torch.FloatTensor(cws_topic).cuda()
        loss_fn_adv = torch.nn.CrossEntropyLoss(weight=cws_topic)
        model_adv = BertClassifier(len(le_topic.classes_), 'bert-base-uncased', adversarial=True)
        model_adv.cuda()
        model_adv.bert_model = model.bert_model

    train(train_dataset, dev_dataset, model, loss_fn, le, model_adv, loss_fn_adv, args.out_model)

    model.load_state_dict(torch.load(args.out_model))
    print("DEV")
    predict(dev_dataset, model, loss_fn, le, print_report=True)
    print("TEST")
    predict(test_dataset, model, loss_fn, le, print_report=True)


if __name__ == "__main__":
    # reproducibility
    torch.manual_seed(42)
    np.random.seed(42)

    parser = argparse.ArgumentParser()
    parser.add_argument('--political_path', type=str, default=None)
    parser.add_argument('--covid_path', type=str, required=True)
    parser.add_argument('--no_sentiment', default=False, action='store_true')
    parser.add_argument('--no_role', default=False, action='store_true')
    parser.add_argument('--adversarial', default=False, action='store_true')
    parser.add_argument('--out_model', type=str, required=True)
    args = parser.parse_args()
    main(args)
